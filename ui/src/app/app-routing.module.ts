import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddBookCollectionComponent } from './pages/add-book-collection/add-book-collection.component';
import { BookDetailComponent } from './pages/book-detail/book-detail.component';
import { BooksComponent } from './pages/books/books.component';
import { HomeComponent } from './pages/home/home.component';
import { ByCategoryComponent } from './pages/lists/by-category/by-category.component';
import { ListsComponent } from './pages/lists/lists.component';
import { LoanedAuthorComponent } from './pages/lists/loaned-author/loaned-author.component';
import { LoanedBookComponent } from './pages/lists/loaned-book/loaned-book.component';
import { LoanedCategoryComponent } from './pages/lists/loaned-category/loaned-category.component';
import { LoanedTimeComponent } from './pages/lists/loaned-time/loaned-time.component';
import { NotReturnedComponent } from './pages/lists/not-returned/not-returned.component';
import { PublishedCatComponent } from './pages/lists/published-cat/published-cat.component';
import { PublishedTimeComponent } from './pages/lists/published-time/published-time.component';
import { TimeLoanedComponent } from './pages/lists/time-loaned/time-loaned.component';
import { UserTypeComponent } from './pages/lists/user-type/user-type.component';
import { LoanBookComponent } from './pages/loan-book/loan-book.component';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { PaysComponent } from './pages/pays/pays.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RegisterComponent } from './pages/register/register.component';
import { CountCategoryComponent } from './pages/reports/count-category/count-category.component';
import { DashboardComponent } from './pages/reports/dashboard/dashboard.component';
import { DebtByCityComponent } from './pages/reports/debt-by-city/debt-by-city.component';
import { DebtByUserTypeComponent } from './pages/reports/debt-by-user-type/debt-by-user-type.component';
import { PayByCityComponent } from './pages/reports/pay-by-city/pay-by-city.component';
import { PayByNameComponent } from './pages/reports/pay-by-name/pay-by-name.component';
import { PayByPayTypeComponent } from './pages/reports/pay-by-pay-type/pay-by-pay-type.component';
import { PayByUserTypeComponent } from './pages/reports/pay-by-user-type/pay-by-user-type.component';
import { ProfitByMonthComponent } from './pages/reports/profit-by-month/profit-by-month.component';
import { ProfitByUserTypeComponent } from './pages/reports/profit-by-user-type/profit-by-user-type.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { SearchComponent } from './pages/search/search.component';
import { UserSettingsComponent } from './pages/user-settings/user-settings.component';
import { UsersComponent } from './pages/users/users.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'my-books', component: BooksComponent },
  { path: 'pays', component: PaysComponent },
  { path: 'lists', component: ListsComponent,
      children: [
        { path: 'loanedbook', component: LoanedBookComponent },
        { path: 'loanedauthor', component: LoanedAuthorComponent },
        { path: 'loanedcategory', component: LoanedCategoryComponent },
        { path: 'loanedtime', component: LoanedTimeComponent },
        { path: 'bycategory', component: ByCategoryComponent },
        { path: 'publishedcat', component: PublishedCatComponent },
        { path: 'publishedtime', component: PublishedTimeComponent },
        { path: 'usertype', component: UserTypeComponent },
        { path: 'notreturned', component: NotReturnedComponent },
        { path: 'timeloaned', component: TimeLoanedComponent }
      ]
  },
  { path: 'users', component: UsersComponent },
  { path: 'reports', component: ReportsComponent, 
      children: [
        { path: 'all', component: DashboardComponent },
        { path: 'payusertype', component: PayByUserTypeComponent },
        { path: 'paycity', component: PayByCityComponent },
        { path: 'debtusertype', component: DebtByUserTypeComponent },
        { path: 'debtcity', component: DebtByCityComponent },
        { path: 'payname', component: PayByNameComponent },
        { path: 'paytype', component: PayByPayTypeComponent },
        { path: 'profitusertype', component: ProfitByUserTypeComponent },
        { path: 'profitmonth', component: ProfitByMonthComponent },
        { path: 'countcategory', component: CountCategoryComponent }
      ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'user-settings', component: UserSettingsComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'search', component: SearchComponent },
  { path: 'search/:key', component: SearchComponent },
  { path: 'add-book', component: AddBookCollectionComponent },
  { path: 'profile/:id', component: ProfileComponent },
  { path: 'detail/:id', component: BookDetailComponent },
  { path: 'loan/:id', component: LoanBookComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
