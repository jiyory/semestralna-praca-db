import { Injectable } from '@angular/core';
import urlSlug from 'url-slug';

@Injectable({
  providedIn: 'root'
})
export class FunctionService {

  public constructor() { }

  public webalize(str: string): string {
    return urlSlug(str);
  }

  // https://stackoverflow.com/questions/46155/whats-the-best-way-to-validate-an-email-address-in-javascript
  public validateEmail(email: string): boolean {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  public convertToBinary(file: File, fnc: Function): void {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      fnc(reader.result);
    };
  }
}
