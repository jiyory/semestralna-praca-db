import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private id: number = -1;
  private name: string = "";
  private lname: string = "";
  private mail: string = "";
  private address: string = "";
  private city: string = "";
  private postal: string = "";
  private type: number = -1;
  private privilege: number = -1;
  private image: string = "";
  
  public constructor() { }

  public isLogged(): boolean {
    return this.id !== -1;
  }

  public unsetData(): void {
    this.id = -1;
    this.name = "";
    this.lname = "";
    this.mail = "";
    this.address = "";
    this.city = "";
    this.postal = "";
    this.type = 0;
    this.privilege = 0;
  }

  public setData(id: number, name: string, lname: string, mail: string, address: string, city: string, postal: string, type: number, privilege: number): void {
    this.id = id;
    this.name = name;
    this.lname = lname;
    this.mail = mail;
    this.address = address;
    this.city = city;
    this.postal = postal;
    this.type = type;
    this.privilege = privilege;
  }

  public setImage(image: string): void {
    this.image = image;
  }

  public getImage(): string {
    return this.image;
  }

  public getID(): number {
    return this.id;
  }

  public getName(): string {
    return this.name;
  }

  public getLastName(): string {
    return this.lname;
  }

  public getMail(): string {
    return this.mail;
  }

  public getFullAddress(): string {
    return this.address + ", " + this.postal + " " + this.city;
  }

  public getAddress(): string {
    return this.address;
  }

  public getCity(): string {
    return this.city;
  }

  public getPostal(): string {
    return this.postal;
  }

  public getType(): number {
    return this.type;
  }

  public getPrivilege(): number {
    return this.privilege;
  }
}
