import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookCollComponent } from './book-coll.component';

describe('BookCollComponent', () => {
  let component: BookCollComponent;
  let fixture: ComponentFixture<BookCollComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookCollComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookCollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
