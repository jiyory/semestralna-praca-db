import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-book-coll',
  templateUrl: './book-coll.component.html',
  styleUrls: ['./book-coll.component.scss']
})
export class BookCollComponent implements OnInit {

  @Input() public index: number = 0;
  @Input() public id_collection: number = -1;
  @Input() public title: string = "";
  @Input() public description: string = "";
  @Input() public isbn: string = "";
  @Input() public publisher: string = "";
  @Input() public year: number = -1;
  @Input() public id_author: number | null = -1;
  @Input() public auth_name: string | null = "";
  @Input() image: string | undefined = undefined;

  public constructor(public user: UserService, private http: RequestService, private toast: ToastrService) { }

  public ngOnInit(): void {
    console.log(this.user.getPrivilege());
  }

  reserve(): void {
    this.http.get("/book/reserveBook/" + this.user.getID() + "/" + this.id_collection).subscribe(
      response => {
        if (response.success) {
          this.toast.success("Kniha rezervovaná.");
        } else {
          this.toast.error("Nepodarilo sa rezervovať. Pravdepodobne už máte knihu rezervovanú.");
        }
      },
      error => {}
    );
  }

}
