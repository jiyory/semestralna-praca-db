import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInMenuComponent } from './user-in-menu.component';

describe('UserInMenuComponent', () => {
  let component: UserInMenuComponent;
  let fixture: ComponentFixture<UserInMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserInMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
