import { Component, OnInit } from '@angular/core';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-in-menu',
  templateUrl: './user-in-menu.component.html',
  styleUrls: ['./user-in-menu.component.scss']
})
export class UserInMenuComponent implements OnInit {

  public faUser = faUser;

  public constructor(public user: UserService, private req: RequestService, private toast: ToastrService, private cookie: CookieService) { }

  public ngOnInit(): void {
    this.req.get("/user/load").subscribe(
      response => {
        if (response.success) {
          this.user.setData(response.data.ID_USER, response.data.FIRST_NAME, response.data.LAST_NAME, response.data.MAIL, 
              response.data.ADDRESS, response.data.CITY, response.data.POSTAL, response.data.ID_USERTYPE, response.data.ID_PRIVILEGE);
          if (response.image !== undefined) {
            this.user.setImage(response.image);
          }
        } else if (response.withToast) {
          this.toast.warning(response.message);
        }
      },
      error => { }
    );
  }

}
