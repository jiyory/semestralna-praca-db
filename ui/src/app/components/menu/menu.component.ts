import { Component, OnInit } from '@angular/core';
import { faBars, faBook, faCoins, faDatabase, faHome, faList, faTable, faUsers } from '@fortawesome/free-solid-svg-icons';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public faHome = faHome;
  public faBook = faBook;
  public faPay = faCoins;
  public faRank = faBars;
  public faUsers = faUsers;
  public faList = faList;
  faData = faDatabase;

  public constructor(private req: RequestService, private toast: ToastrService, public user: UserService) { }

  public ngOnInit(): void {
  }

  public updateDatabase(): void {
    this.update();
  }

  public resetDatabase(): void {
    this.req.get("/db/reset").subscribe(
      response => {
        this.toast.success(response.message);
      },
      error => { }
    );
  }

  private update(): void {
    this.toast.warning("Spúšťam update...");
    this.req.get("/db/update").subscribe(
      response => {
        console.log(response);
        if (response.success) {
          if (response.warning) {
            this.toast.warning(response.message);
          } else {
            this.toast.success(response.message);
          }
          this.update();
        } else {
          this.toast.success(response.message);
        }
      },
      error => { }
    );
  }
}
