import { Component, OnInit } from '@angular/core';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-top-settings',
  templateUrl: './top-settings.component.html',
  styleUrls: ['./top-settings.component.scss']
})
export class TopSettingsComponent implements OnInit {

  public faSettings = faCog;

  public constructor(public user: UserService) { }

  public ngOnInit(): void {
  }

}
