import { Component, Input, OnInit } from '@angular/core';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  math = Math;
  faPrev = faArrowLeft;
  faNext = faArrowRight;

  @Input() title: string = "";
  @Input() header: any[] = [];
  @Input() data: any[] = [];
  @Input() perPage: number = 10;
  @Input() search: boolean = false;
  @Input() searchF?: any;
  @Input() link: boolean = false;
  @Input() linkUrl: string = "";
  @Input() linkPar: string = "";
  @Input() buttons: boolean = false;
  @Input() buttonsVar: any[] = [];

  searchVar: string = "";

  keys: any;
  page: number = 1;
  maxPage: number = 1;

  constructor() { }

  ngOnInit(): void {
    if (this.header.length > 0) {
      this.keys = Object.keys(this.header[0]);
    }
  }

  next(): void {
    if (this.page < Math.ceil(this.data.length / this.perPage))
      ++this.page;
  }

  prev(): void {
    if (this.page > 1)
      --this.page;
  }

  onChange(): void {
    if (this.searchF !== undefined) {
      this.page = 1;
      this.searchF(this.searchVar);
    }
  }
}
