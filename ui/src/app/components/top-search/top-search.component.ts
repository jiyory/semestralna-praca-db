import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FunctionService } from 'src/app/services/function.service';

@Component({
  selector: 'app-top-search',
  templateUrl: './top-search.component.html',
  styleUrls: ['./top-search.component.scss']
})
export class TopSearchComponent implements OnInit {

  public searchText: string = "";

  public faSearch = faSearch;

  private timer?: any;

  constructor(private func: FunctionService, private route: Router) { }

  public ngOnInit(): void {
  }

  public onChange(): void {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.search();
    }, 333);
  }

  private search(): void {
    this.route.navigate(['/search/' + this.func.webalize(this.searchText)]);
  }
}
