import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app/app.component';
import { MenuComponent } from './components/menu/menu.component';
import { TopPanelComponent } from './components/top-panel/top-panel.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { LogoComponent } from './components/logo/logo.component';
import { UserInMenuComponent } from './components/user-in-menu/user-in-menu.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TopSearchComponent } from './components/top-search/top-search.component';
import { TopSettingsComponent } from './components/top-settings/top-settings.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './pages/search/search.component';
import { BookCollComponent } from './components/book-coll/book-coll.component';
import { AddBookCollectionComponent } from './pages/add-book-collection/add-book-collection.component';
import { UserSettingsComponent } from './pages/user-settings/user-settings.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { BooksComponent } from './pages/books/books.component';
import { PaysComponent } from './pages/pays/pays.component';
import { ListsComponent } from './pages/lists/lists.component';
import { UsersComponent } from './pages/users/users.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { TableComponent } from './components/table/table.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { BookDetailComponent } from './pages/book-detail/book-detail.component';
import { LoanBookComponent } from './pages/loan-book/loan-book.component';
import { DashboardComponent } from './pages/reports/dashboard/dashboard.component';
import { PayByUserTypeComponent } from './pages/reports/pay-by-user-type/pay-by-user-type.component';
import { PayByCityComponent } from './pages/reports/pay-by-city/pay-by-city.component';
import { PayByNameComponent } from './pages/reports/pay-by-name/pay-by-name.component';
import { PayByPayTypeComponent } from './pages/reports/pay-by-pay-type/pay-by-pay-type.component';
import { DebtByUserTypeComponent } from './pages/reports/debt-by-user-type/debt-by-user-type.component';
import { DebtByCityComponent } from './pages/reports/debt-by-city/debt-by-city.component';
import { ProfitByUserTypeComponent } from './pages/reports/profit-by-user-type/profit-by-user-type.component';
import { ProfitByMonthComponent } from './pages/reports/profit-by-month/profit-by-month.component';
import { CountCategoryComponent } from './pages/reports/count-category/count-category.component';
import { TimeLoanedComponent } from './pages/lists/time-loaned/time-loaned.component';
import { NotReturnedComponent } from './pages/lists/not-returned/not-returned.component';
import { UserTypeComponent } from './pages/lists/user-type/user-type.component';
import { PublishedTimeComponent } from './pages/lists/published-time/published-time.component';
import { PublishedCatComponent } from './pages/lists/published-cat/published-cat.component';
import { ByCategoryComponent } from './pages/lists/by-category/by-category.component';
import { LoanedTimeComponent } from './pages/lists/loaned-time/loaned-time.component';
import { LoanedCategoryComponent } from './pages/lists/loaned-category/loaned-category.component';
import { LoanedAuthorComponent } from './pages/lists/loaned-author/loaned-author.component';
import { LoanedBookComponent } from './pages/lists/loaned-book/loaned-book.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    TopPanelComponent,
    ContentComponent,
    FooterComponent,
    LogoComponent,
    UserInMenuComponent,
    TopSearchComponent,
    TopSettingsComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    SearchComponent,
    BookCollComponent,
    AddBookCollectionComponent,
    UserSettingsComponent,
    LogoutComponent,
    BooksComponent,
    PaysComponent,
    ListsComponent,
    UsersComponent,
    ReportsComponent,
    TableComponent,
    ProfileComponent,
    BookDetailComponent,
    LoanBookComponent,
    DashboardComponent,
    PayByUserTypeComponent,
    PayByCityComponent,
    PayByNameComponent,
    PayByPayTypeComponent,
    DebtByUserTypeComponent,
    DebtByCityComponent,
    ProfitByUserTypeComponent,
    ProfitByMonthComponent,
    CountCategoryComponent,
    TimeLoanedComponent,
    NotReturnedComponent,
    UserTypeComponent,
    PublishedTimeComponent,
    PublishedCatComponent,
    ByCategoryComponent,
    LoanedTimeComponent,
    LoanedCategoryComponent,
    LoanedAuthorComponent,
    LoanedBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
