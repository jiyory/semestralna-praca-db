import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-time-loaned',
  templateUrl: './time-loaned.component.html',
  styleUrls: ['./time-loaned.component.scss']
})
export class TimeLoanedComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ ROWNUM: "Poradie", TITUL: "Názov", DNI_POZICANE: "Požičaných dní" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/list/mostTimeLoanedBook").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }
    );
  }

}
