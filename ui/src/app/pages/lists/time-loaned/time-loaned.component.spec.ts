import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeLoanedComponent } from './time-loaned.component';

describe('TimeLoanedComponent', () => {
  let component: TimeLoanedComponent;
  let fixture: ComponentFixture<TimeLoanedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TimeLoanedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeLoanedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
