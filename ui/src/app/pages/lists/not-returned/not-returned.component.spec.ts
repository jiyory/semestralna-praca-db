import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotReturnedComponent } from './not-returned.component';

describe('NotReturnedComponent', () => {
  let component: NotReturnedComponent;
  let fixture: ComponentFixture<NotReturnedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotReturnedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotReturnedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
