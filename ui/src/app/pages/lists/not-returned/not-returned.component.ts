import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-not-returned',
  templateUrl: './not-returned.component.html',
  styleUrls: ['./not-returned.component.scss']
})
export class NotReturnedComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ ROWNUM: "Poradie", TITUL: "Názov", POCET_NEVRATENYCH: "Počet neskorých vrátení" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/list/mostNoreturnedBook").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }
    );
  }

}
