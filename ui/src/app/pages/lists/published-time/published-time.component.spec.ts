import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedTimeComponent } from './published-time.component';

describe('PublishedTimeComponent', () => {
  let component: PublishedTimeComponent;
  let fixture: ComponentFixture<PublishedTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishedTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
