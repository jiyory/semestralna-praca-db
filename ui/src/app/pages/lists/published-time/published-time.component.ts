import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-published-time',
  templateUrl: './published-time.component.html',
  styleUrls: ['./published-time.component.scss']
})
export class PublishedTimeComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ 'BOOK_COLL.YEAR': "Rok vydania", CAT_NAME: "Kategória", 'COUNT(ID_COLLECTION)': "Počet vydaných kníh" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/list/publishedBookInCategoryByTime").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }
    );
  }

}
