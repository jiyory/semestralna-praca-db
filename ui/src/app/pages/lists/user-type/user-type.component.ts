import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-user-type',
  templateUrl: './user-type.component.html',
  styleUrls: ['./user-type.component.scss']
})
export class UserTypeComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ RN: "Poradie", TYP_UZIVATELA: "Typ užívateľa", KATEGORIA: "Kategória", POCET_POZICANI: "Počet požičaných kníh" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/list/loanedBookByUser").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }
    );
  }

}
