import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-loaned-category',
  templateUrl: './loaned-category.component.html',
  styleUrls: ['./loaned-category.component.scss']
})
export class LoanedCategoryComponent implements OnInit {

  loading: boolean = true;

  header: any[] = [{ ROWNUMBER: "Poradie", CAT_NAME: "Názov", POCET_POZICANI: "Počet požičaní" }];
  data: any[] = [];

  year: string = "";
  month: string = "";

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    var date = new Date();
    this.year = "" + date.getFullYear();
    this.load();
  }

  load(): void {
    this.http.get("/list/mostLoanedCategory/" + this.year + "/" + this.month).subscribe(
      response => {
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }, error => {}
    );
  }

}
