import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanedCategoryComponent } from './loaned-category.component';

describe('LoanedCategoryComponent', () => {
  let component: LoanedCategoryComponent;
  let fixture: ComponentFixture<LoanedCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanedCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanedCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
