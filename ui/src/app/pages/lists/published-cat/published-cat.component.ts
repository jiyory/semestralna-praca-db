import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-published-cat',
  templateUrl: './published-cat.component.html',
  styleUrls: ['./published-cat.component.scss']
})
export class PublishedCatComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ ROK: "Rok vydania", KATEGORIA: "Kategória", POCET_KNIH: "Počet vydaných kníh" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/list/countPublishedBookByCategory").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }
    );
  }

}
