import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedCatComponent } from './published-cat.component';

describe('PublishedCatComponent', () => {
  let component: PublishedCatComponent;
  let fixture: ComponentFixture<PublishedCatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishedCatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
