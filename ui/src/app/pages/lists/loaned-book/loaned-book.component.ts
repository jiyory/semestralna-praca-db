import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-loaned-book',
  templateUrl: './loaned-book.component.html',
  styleUrls: ['./loaned-book.component.scss']
})
export class LoanedBookComponent implements OnInit {

  loading: boolean = true;

  header: any[] = [{ ROWNUMBER: "Poradie", 'BOOK_COLL.TITLE': "Názov", COUNT: "Počet požičaní" }];
  data: any[] = [];

  year: string = "";
  month: string = "";

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    var date = new Date();
    this.year = "" + date.getFullYear();
    this.load();
  }

  load(): void {
    this.http.get("/list/mostLoanedBook/" + this.year + "/" + this.month).subscribe(
      response => {
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }, error => {}
    );
  }

}
