import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanedBookComponent } from './loaned-book.component';

describe('LoanedBookComponent', () => {
  let component: LoanedBookComponent;
  let fixture: ComponentFixture<LoanedBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanedBookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanedBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
