import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanedTimeComponent } from './loaned-time.component';

describe('LoanedTimeComponent', () => {
  let component: LoanedTimeComponent;
  let fixture: ComponentFixture<LoanedTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanedTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanedTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
