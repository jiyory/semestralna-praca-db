import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-loaned-time',
  templateUrl: './loaned-time.component.html',
  styleUrls: ['./loaned-time.component.scss']
})
export class LoanedTimeComponent implements OnInit {

  loading: boolean = true;

  header: any[] = [{ RN: "Poradie", FULLNAME: "Meno", POCET: "Počet požičaní" }];
  data: any[] = [];

  year: string = "";
  month: string = "";

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    var date = new Date();
    this.year = "" + date.getFullYear();
    this.load();
  }

  load(): void {
    this.http.get("/list/mostLoanedBooksByTime/" + this.year + "/" + this.month).subscribe(
      response => {
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }, error => {}
    );
  }
}
