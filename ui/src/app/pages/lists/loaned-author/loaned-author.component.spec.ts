import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanedAuthorComponent } from './loaned-author.component';

describe('LoanedAuthorComponent', () => {
  let component: LoanedAuthorComponent;
  let fixture: ComponentFixture<LoanedAuthorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanedAuthorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanedAuthorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
