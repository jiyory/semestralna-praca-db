import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-loaned-author',
  templateUrl: './loaned-author.component.html',
  styleUrls: ['./loaned-author.component.scss']
})
export class LoanedAuthorComponent implements OnInit {

  loading: boolean = true;

  header: any[] = [{ ROWNUMBER: "Poradie", COLUMN_VALUE: "Názov", COUNT: "Počet požičaní" }];
  data: any[] = [];

  year: string = "";
  month: string = "";

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    var date = new Date();
    this.year = "" + date.getFullYear();
    this.load();
  }

  load(): void {
    this.http.get("/list/mostLoanedAuthor/" + this.year + "/" + this.month).subscribe(
      response => {
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }, error => {}
    );
  }

}
