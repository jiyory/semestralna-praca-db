import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  loading: boolean = true;
  pozicane?: any;
  historia?: any;
  rezervovane?: any;

  pozicaneHeader: any[] = [
    { TITLE: "Názov", YEAR: "Rok vydania", PUBLISHER: "Vydavateľstvo", DATE_FROM: "Požičaná dňa", DATE_TO: "Požičané do" }
  ];

  historiaHeader: any[] = [
    { TITLE: "Názov", YEAR: "Rok vydania", PUBLISHER: "Vydavateľstvo", DATE_FROM: "Požičaná dňa", DATE_TO: "Požičané do", RETURN_DATE: "Vrátené dňa" }
  ];

  rezervavaneHeader: any[] = [
    { TITLE: "Názov", YEAR: "Rok vydania", PUBLISHER: "Vydavateľstvo", DATE_FROM: "Dátum rezervácie" }
  ];

  constructor(private http: RequestService, private user: UserService, private route: Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      if (!this.user.isLogged()) {
        this.route.navigate(['/home']);
      } else {
        this.http.get("/users/userDetail/" + this.user.getID()).subscribe(
          response => {
            this.pozicane = response.data.pozicane;
            this.historia = response.data.historia;
            this.rezervovane = response.data.rezervovane;
            this.loading = false;
          },
          error => {}
        );
      }
    }, 1000);
  }

}
