import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {

  loading: boolean = true;
  book?: any;
  volne?: any;
  rezervovane?: any;
  pozicane?: any;
  historia?: any;
  platnost?: boolean;
  pocet: number = 0;

  rack: string = "";
  rack_num: number = 1;

  volneHeader: any[] = [
    { LABEL: "Názov knižnice", SECTION: "Sekcia", LEVEL_OF: "Polička" }
  ];

  rezervavaneHeader: any[] = [
    { FULLNAME: "Meno", MAIL: "Email", DATE_FROM: "Rezervované dňa" }
  ];

  pozicaneHeader: any[] = [
    { FULLNAME: "Meno", MAIL: "Email", DATE_FROM: "Požičané od", DATE_TO: "Požičané do" }
  ];

  historiaHeader: any[] = [
    { FULLNAME: "Meno", MAIL: "Email", DATE_FROM: "Požičané od", DATE_TO: "Požičané do", RETURN_DATE: "Dátum vrátenia" }
  ];

  rezervovaneButton: any[] = [];
  pozicaneButton: any[] = [];
  volneButton: any[] = [];

  constructor(private http: RequestService, private route: ActivatedRoute, private user: UserService, private toast: ToastrService) { }

  ngOnInit(): void {
    this.rezervovaneButton = [
      { text: "Požičať", action: this.pozicat, param: "ID_USER" }
    ];
    this.pozicaneButton = [
      { text: "Vrátiť", action: this.vratit, params: ["ID_USER", "ID_BOOK"] }
    ];
    this.volneButton = [
      { text: "Odstrániť", action: this.odstranit, param: "ID_BOOK" }
    ];
    this.loadData();
  }

  loadData(): void {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get("/book/detailBook/" + id).subscribe(
      response => {
        console.log(response);
        this.book = response.data.profile[0];
        this.pocet = response.data.inLibrary.length;
        this.volne = response.data.inLibrary;
        this.rezervovane = response.data.reservation;
        this.historia = response.data.historia;
        this.pozicane = response.data.nevratene;
        this.loading = false;
      },
      error => {}
    );
  }

  pozicat = (id: any): void => {
    var param = this.route.snapshot.paramMap.get('id');
    this.http.get("/book/loanBook/" + id + "/" + param).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.loadData();
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }

  vratit = (param: any): void => {
    this.http.get("/book/returnBook/" + param.ID_USER + "/" + param.ID_BOOK).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.loadData();
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }

  addBook(): void {
    var param = this.route.snapshot.paramMap.get('id');
    this.http.get("/book/addBook/" + this.rack + "/" + this.rack_num + "/" + param).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.loadData();
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }

  odstranit = (param: any): void => {
    this.http.get("/book/removeBook/" + param).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.loadData();
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }
}
