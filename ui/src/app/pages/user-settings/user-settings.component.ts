import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FunctionService } from 'src/app/services/function.service';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {

  public file?: Blob;
  public name: string = "";
  public last_name: string = "";
  public address: string = "";
  public postal_code: string = "";
  public city: string = "";
  public mail: string = "";
  public password: string = "";
  public password_ag: string = "";

  public constructor(private user: UserService, private route: Router, private toast: ToastrService, private f: FunctionService, private req: RequestService) { }

  public ngOnInit(): void {
    setTimeout(() => {
      if (!this.user.isLogged()) {
        this.route.navigate(['/home']);
      } else {
        this.name = this.user.getName();
        this.mail = this.user.getMail();
        this.last_name = this.user.getLastName();
        this.address = this.user.getAddress();
        this.postal_code = this.user.getPostal();
        this.city = this.user.getCity();
        this.password = "";
      }
    }, 1000);
  }

  public submit(): void {
    if (this.checkInputs()) {
      var data: any = {
        name: this.name,
        lname: this.last_name,
        address: this.address,
        postal: this.postal_code,
        city: this.city,
        mail: this.mail
      };
      if (this.password.length > 0) {
        data.pwd = this.password;
      }
      if (this.file !== undefined) {
        data.image = this.file;
      }
      this.req.post("/user/update", data).subscribe(
        response => {
          if (response.success) {
            this.toast.success(response.message);
            this.user.setData(response.data.ID_USER, response.data.FIRST_NAME, response.data.LAST_NAME, response.data.MAIL, 
              response.data.ADDRESS, response.data.CITY, response.data.POSTAL, response.data.ID_USERTYPE, response.data.ID_PRIVILEGE);
            if (response.image !== undefined) {
              this.user.setImage(response.image);
            }
          } else {
            this.toast.warning(response.message);
          }
        },
        error => { }
      );
    }
  }

  public onChangeFile(event: any): void {
    this.f.convertToBinary(event.target.files[0], (data: Blob) => {
      this.file = data;
    });
  }

  private checkInputs(): boolean {
    var success = true;

    if (this.postal_code.length !== 5) {
      success = false;
      this.toast.error('PSČ musí obsahovať 5 číslic!');
    }

    if (this.city.length < 3) {
      success = false;
      this.toast.error('Mesto musí obsahovať aspoň 3 znaky.');
    }

    if (this.address.length < 1) {
      success = false;
      this.toast.error('Ulica musí obsahovať aspoň 1 znak.');
    }

    if (this.password.length < 8 && this.password.length > 0) {
      success = false;
      this.toast.error('Heslo je príliš krátke. Minimálne 8 znakov.');
    } else if (this.password !== this.password_ag) {
      success = false;
      this.toast.error('Heslá sa nezhodujú!');
    }

    if (this.mail.length < 5 || !this.f.validateEmail(this.mail)) {
      success = false;
      this.toast.error('Email má nesprávy formát!');
    }

    if (this.last_name.length < 3 || this.last_name[0] !== this.last_name[0].toUpperCase()) {
      success = false;
      this.toast.error('Priezvisko je príliš krátke alebo nezačína veľkým písmenom!');
    }

    if (this.name.length < 3 || this.name[0] !== this.name[0].toUpperCase()) {
      success = false;
      this.toast.error('Meno je príliš krátke alebo nezačína veľkým písmenom!');
    }

    return success;
  }
}
