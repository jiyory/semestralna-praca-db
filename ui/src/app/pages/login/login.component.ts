import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { FunctionService } from 'src/app/services/function.service';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public mail: string = "";
  public password: string = "";

  public constructor(private user: UserService, private route: Router, private req: RequestService, private f: FunctionService, 
      private toast: ToastrService, private cookie: CookieService) { }

  public ngOnInit(): void {
    if (this.user.isLogged()) {
      this.route.navigate(['/home']);
    } else {

    }
  }

  public submit(): void {
    if (this.checkInputs()) {
      this.req.post("/user/login", {
        mail: this.mail,
        pwd: this.password
      }).subscribe(
        response => {
          if (response.success) {
            this.toast.success(response.message);
            this.user.setData(response.data.ID_USER, response.data.FIRST_NAME, response.data.LAST_NAME, response.data.MAIL, 
                response.data.ADDRESS, response.data.CITY, response.data.POSTAL, response.data.ID_USERTYPE, response.data.ID_PRIVILEGE);
            if (response.image !== undefined) {
              this.user.setImage(response.image);
            }
            this.cookie.set("SID", response.sessID);
            this.route.navigate(['/home']);
          } else {
            this.toast.warning(response.message);
          }
        },
        error => { }
      );
    }
  }

  private checkInputs(): boolean {
    var success = true;

    if (this.password.length < 8) {
      success = false;
      this.toast.error('Heslo je príliš krátke. Minimálne 8 znakov.');
    }

    if (this.mail.length < 5 || !this.f.validateEmail(this.mail)) {
      success = false;
      this.toast.error('Email má nesprávy formát!');
    }

    return success;
  }
}
