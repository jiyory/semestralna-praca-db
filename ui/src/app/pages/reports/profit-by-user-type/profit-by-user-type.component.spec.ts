import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitByUserTypeComponent } from './profit-by-user-type.component';

describe('ProfitByUserTypeComponent', () => {
  let component: ProfitByUserTypeComponent;
  let fixture: ComponentFixture<ProfitByUserTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfitByUserTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitByUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
