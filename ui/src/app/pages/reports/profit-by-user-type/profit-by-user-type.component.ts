import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-profit-by-user-type',
  templateUrl: './profit-by-user-type.component.html',
  styleUrls: ['./profit-by-user-type.component.scss']
})
export class ProfitByUserTypeComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ TYPE: "Typ užívateľa",  DATUM: "Dátum", ZISK: "Hodnota" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/report/profitByMounthUsertype").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.data.forEach((val) => {
          val.ZISK = Math.round(val.ZISK * 1000) / 1000 + " €";
        });
        this.loading = false;
      }
    );
  }

}
