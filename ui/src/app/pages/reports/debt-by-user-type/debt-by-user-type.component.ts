import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-debt-by-user-type',
  templateUrl: './debt-by-user-type.component.html',
  styleUrls: ['./debt-by-user-type.component.scss']
})
export class DebtByUserTypeComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ TYPE: "Typ používateľa", SUMA: "Hodnota" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/report/debtByUsertype").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.data.forEach((val) => {
          val.SUMA = Math.round(val.SUMA * 1000) / 1000 + " €";
        });
        this.loading = false;
      }
    );
  }
}
