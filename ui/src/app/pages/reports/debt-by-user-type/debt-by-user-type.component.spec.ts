import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtByUserTypeComponent } from './debt-by-user-type.component';

describe('DebtByUserTypeComponent', () => {
  let component: DebtByUserTypeComponent;
  let fixture: ComponentFixture<DebtByUserTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebtByUserTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtByUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
