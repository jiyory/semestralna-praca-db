import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitByMonthComponent } from './profit-by-month.component';

describe('ProfitByMonthComponent', () => {
  let component: ProfitByMonthComponent;
  let fixture: ComponentFixture<ProfitByMonthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfitByMonthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitByMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
