import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-profit-by-month',
  templateUrl: './profit-by-month.component.html',
  styleUrls: ['./profit-by-month.component.scss']
})
export class ProfitByMonthComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ DATUM: "Dátum", ZISK: "Hodnota" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/report/profitByMounth").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.data.forEach((val) => {
          val.ZISK = Math.round(val.ZISK * 1000) / 1000 + " €";
        });
        this.loading = false;
      }
    );
  }

}
