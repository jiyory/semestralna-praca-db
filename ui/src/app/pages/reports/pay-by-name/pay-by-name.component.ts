import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-pay-by-name',
  templateUrl: './pay-by-name.component.html',
  styleUrls: ['./pay-by-name.component.scss']
})
export class PayByNameComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ FULLNAME: "Meno používateľa", MAIL: "Email", VALUE: "Hodnota", DATE_PAY: "Dátum platby", DESCRIPTION: "Popis" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.searchFunc(null);
  }

  searchFunc = (text: string | null): void => {
    if (text !== null && text?.length < 1) text = null;
    this.http.get("/report/payByName/" + text).subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.data.forEach((val) => {
          val.VALUE = Math.round(val.VALUE * 1000) / 1000 + " €";
        });
        this.loading = false;
      }
    );
  }

}
