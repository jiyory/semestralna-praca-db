import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayByNameComponent } from './pay-by-name.component';

describe('PayByNameComponent', () => {
  let component: PayByNameComponent;
  let fixture: ComponentFixture<PayByNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayByNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayByNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
