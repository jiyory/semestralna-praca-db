import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtByCityComponent } from './debt-by-city.component';

describe('DebtByCityComponent', () => {
  let component: DebtByCityComponent;
  let fixture: ComponentFixture<DebtByCityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DebtByCityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtByCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
