import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountCategoryComponent } from './count-category.component';

describe('CountCategoryComponent', () => {
  let component: CountCategoryComponent;
  let fixture: ComponentFixture<CountCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
