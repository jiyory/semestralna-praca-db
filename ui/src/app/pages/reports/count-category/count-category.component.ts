import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-count-category',
  templateUrl: './count-category.component.html',
  styleUrls: ['./count-category.component.scss']
})
export class CountCategoryComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ CAT_NAME: "Kategória",  VOLNEMIESTA: "Počet voľných miest" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/report/countPlaceByCategory").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.loading = false;
      }
    );
  }

}
