import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayByPayTypeComponent } from './pay-by-pay-type.component';

describe('PayByPayTypeComponent', () => {
  let component: PayByPayTypeComponent;
  let fixture: ComponentFixture<PayByPayTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayByPayTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayByPayTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
