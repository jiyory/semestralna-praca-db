import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-pay-by-city',
  templateUrl: './pay-by-city.component.html',
  styleUrls: ['./pay-by-city.component.scss']
})
export class PayByCityComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ CITY: "Názov mesta", SUMA: "Hodnota" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/report/paidByCity").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.data.forEach((val) => {
          val.SUMA = Math.round(val.SUMA * 1000) / 1000 + " €";
        });
        this.loading = false;
      }
    );
  }

}
