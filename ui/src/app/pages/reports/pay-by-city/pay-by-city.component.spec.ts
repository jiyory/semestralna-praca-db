import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayByCityComponent } from './pay-by-city.component';

describe('PayByCityComponent', () => {
  let component: PayByCityComponent;
  let fixture: ComponentFixture<PayByCityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayByCityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayByCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
