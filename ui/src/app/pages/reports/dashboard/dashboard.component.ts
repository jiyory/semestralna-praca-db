import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  pozicanych: string = "";
  knih: any = 0;
  aktivny: any = 0;
  registrovany: any = 0;
  zisk: any = 0;
  loading: boolean = true;

  header: any[] = [{ nazov: "Názov", hodnota: "Hodnota" }];
  data: any[] = [];

  dateFrom: string = "";
  dateTo: string = "";

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    var date = new Date();
    date.setMonth(0); // month zacina 0 nie 1
    date.setDate(1);
    this.dateFrom = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
    date = new Date();
    this.dateTo = date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear();
    this.load();
  }

  load(): void {
    this.http.get("/report/overall/" + this.dateFrom + "/" + this.dateTo).subscribe(
      response => {
        this.pozicanych = response.data.pozicanych;
        this.knih = response.data.knihy;
        this.zisk = response.data.profit;
        this.registrovany = response.data.registrovany;
        this.aktivny = response.data.aktivny;

        this.data = [
          { nazov: "Počet kníh v knižnici", hodnota: this.knih },
          { nazov: "Celkový zisk v knižnici", hodnota: this.zisk },
          { nazov: "Počet požičaných kníh za obodobie", hodnota: this.pozicanych },
          { nazov: "Počet nových užívateľov za obdobie", hodnota: this.registrovany },
          { nazov: "Počet aktívnzch užívateľov za obdobie", hodnota: this.aktivny }
        ];
        this.loading = false;
      }, error => {}
    );
  }

}
