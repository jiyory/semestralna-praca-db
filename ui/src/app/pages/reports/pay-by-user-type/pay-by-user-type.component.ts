import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-pay-by-user-type',
  templateUrl: './pay-by-user-type.component.html',
  styleUrls: ['./pay-by-user-type.component.scss']
})
export class PayByUserTypeComponent implements OnInit {

  loading: boolean = true;
  header: any[] = [{ TYPE: "Typ užívateľa", SUMA: "Hodnota" }];
  data: any[] = [];

  constructor(private http: RequestService) { }

  ngOnInit(): void {
    this.http.get("/report/paidByUsertype").subscribe(
      response => { 
        console.log(response);
        this.data = response.data;
        this.data.forEach((val) => {
          val.SUMA = Math.round(val.SUMA * 1000) / 1000 + " €";
        });
        this.loading = false;
      }
    );
  }

}
