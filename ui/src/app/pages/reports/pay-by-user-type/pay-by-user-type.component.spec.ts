import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PayByUserTypeComponent } from './pay-by-user-type.component';

describe('PayByUserTypeComponent', () => {
  let component: PayByUserTypeComponent;
  let fixture: ComponentFixture<PayByUserTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PayByUserTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PayByUserTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
