import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FunctionService } from 'src/app/services/function.service';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public name: string = "";
  public last_name: string = "";
  public address: string = "";
  public postal_code: string = "";
  public city: string = "";
  public mail: string = "";
  public password: string = "";
  public password_ag: string = "";
  public user_type: number = 0;

  public usertype: any[] = [];
 

  public constructor(private f: FunctionService, private toast: ToastrService, private req: RequestService, private route: Router, private user: UserService) { }

  public ngOnInit(): void {
    if (this.user.isLogged()) {
      this.route.navigate(['/home']);
    } else {
      this.req.get("/user/usertype").subscribe(
        success => {
          this.usertype = success.data;
        },
        error => { }
      );
    }
  }

  public submit(): void {
    if (this.checkInputs()) {
      const data = {
        name: this.name,
        last_name: this.last_name,
        address: this.address,
        postal_code: this.postal_code,
        city: this.city,
        mail: this.mail,
        password: this.password,
        usertype: this.user_type
      };
      this.req.post("/user/register", data).subscribe(
        success => {
          if (success.created) {
            this.toast.success(success.message);
            this.clean();
            this.route.navigate(['/login']);
          } else {
            this.toast.warning(success.message);
          }
        },
        error => { }
      );
    }
  }

  private clean(): void {
    this.name = "";
    this.last_name = "";
    this.mail = "";
    this.password = "";
    this.password_ag = "";
    this.address = "";
    this.city = "";
    this.postal_code = "";
    this.user_type = 0;
  }

  private checkInputs(): boolean {
    var success = true;

    if (this.user_type === 0) {
      success = false;
      this.toast.error('Musíte zvoliť typ!');
    }

    if (this.postal_code.length !== 5) {
      success = false;
      this.toast.error('PSČ musí obsahovať 5 číslic!');
    }

    if (this.city.length < 3) {
      success = false;
      this.toast.error('Mesto musí obsahovať aspoň 3 znaky.');
    }

    if (this.address.length < 1) {
      success = false;
      this.toast.error('Ulica musí obsahovať aspoň 1 znak.');
    }

    if (this.password.length < 8) {
      success = false;
      this.toast.error('Heslo je príliš krátke. Minimálne 8 znakov.');
    } else if (this.password !== this.password_ag) {
      success = false;
      this.toast.error('Heslá sa nezhodujú!');
    }

    if (this.mail.length < 5 || !this.f.validateEmail(this.mail)) {
      success = false;
      this.toast.error('Email má nesprávy formát!');
    }

    if (this.last_name.length < 3 || this.last_name[0] !== this.last_name[0].toUpperCase()) {
      success = false;
      this.toast.error('Priezvisko je príliš krátke alebo nezačína veľkým písmenom!');
    }

    if (this.name.length < 3 || this.name[0] !== this.name[0].toUpperCase()) {
      success = false;
      this.toast.error('Meno je príliš krátke alebo nezačína veľkým písmenom!');
    }

    return success;
  }
}
