import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public key: string | null = null;
  public books: any[] = [];

  public constructor(private route: ActivatedRoute, private router: Router, private req: RequestService) { 
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  public ngOnInit(): void {
    this.key = this.route.snapshot.paramMap.get('key');
    this.search();
  }

  private search(): void {
    const data = { key: this.key };
    this.req.get("/book/search/" + this.key).subscribe(
      response => {
        this.books = response.data;
      },
      error => { }
    );
  }

}
