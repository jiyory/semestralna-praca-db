import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  id: string | null = null;

  loading: boolean = true;
  profile?: any;
  rezervovane?: any;
  pozicane?: any;
  historia?: any;
  platby?: any;
  platnost?: boolean;

  rezervavaneHeader: any[] = [
    { TITLE: "Názov", YEAR: "Rok vydania", PUBLISHER: "Vydavateľstvo", DATE_FROM: "Dátum rezervácie" }
  ];

  pozicaneHeader: any[] = [
    { TITLE: "Názov", YEAR: "Rok vydania", PUBLISHER: "Vydavateľstvo", DATE_FROM: "Požičaná dňa", DATE_TO: "Požičané do" }
  ];

  historiaHeader: any[] = [
    { TITLE: "Názov", YEAR: "Rok vydania", PUBLISHER: "Vydavateľstvo", DATE_FROM: "Požičaná dňa", DATE_TO: "Požičané do", RETURN_DATE: "Vrátené dňa" }
  ];

  platbyHeader: any[] = [
    { DESCRIPTION: "Popis", DATE_PAY: "Dátum platby", VALUE: "Hodnota" }
  ];

  rezervovaneButton: any[] = [];
  pozicaneButton: any[] = [];
  privilege: any[] = [];
  usertype: any[] = [];

  newUserType: number = 1;
  newUserPrivilege: number = 1;

  constructor(private http: RequestService, private route: ActivatedRoute, public user: UserService, private toast: ToastrService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.rezervovaneButton = [
      { text: "Požičať", action: this.pozicat, param: "COLECTIONID" }
    ];
    this.pozicaneButton = [
      { text: "Vrátiť", action: this.vratit, param: "BOOKID" }
    ];
    this.loadData();

    this.http.get("/users/getPrivilegeType").subscribe(
      response => {
        console.log(response);
        this.privilege = response.data;
      },
      error => {}
    );

    this.http.get("/users/getUserType").subscribe(
      response => {
        console.log(response);
        this.usertype = response.data;
      },
      error => {}
    );
  }

  loadData(): void {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get("/users/userDetail/" + id).subscribe(
      response => {
        //console.log(response);
        this.profile = response.data.profile[0];
        this.pozicane = response.data.pozicane;
        this.rezervovane = response.data.rezervovane;
        this.historia = response.data.historia;
        this.platby = response.data.platby;

        this.newUserType = this.profile.ID_USERTYPE;
        this.newUserPrivilege = this.profile.ID_PRIVILEGE;

        this.platby.forEach((item: any) => {
          item.VALUE = item.VALUE + " €";
        });

        this.platnost = response.data.platnost[0].PLATNOST === "true";
        this.loading = false;
      },
      error => {}
    );
  }

  pozicat = (param: any): void => {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get("/book/loanBook/" + id + "/" + param).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.loadData();
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }

  vratit = (param: any): void => {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get("/book/returnBook/" + id + "/" + param).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.loadData();
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }

  setPrivilege(): void {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get("/users/setUserTypePrivilage/" + id + "/" + this.newUserPrivilege + "/" + this.newUserType).subscribe(
      response => {
        if (response.success) {
          this.toast.success("Práva a typ úspešne nastavené.");
          this.loadData();
        } else {
          this.toast.error("Nepodarilo sa nastaviť práva!");
        }
      },
      error => {}
    );
  }
}
