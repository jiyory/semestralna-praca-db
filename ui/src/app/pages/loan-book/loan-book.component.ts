import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FunctionService } from 'src/app/services/function.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-loan-book',
  templateUrl: './loan-book.component.html',
  styleUrls: ['./loan-book.component.scss']
})
export class LoanBookComponent implements OnInit {

  header: any[] = [];
  data: any[] = [];
  loading: boolean = true;

  buttons: any[] = [];
  timer?: any;

  constructor(private http: RequestService, private route: ActivatedRoute, private router: Router, private toast: ToastrService, private f: FunctionService) { }

  ngOnInit(): void {
    this.buttons = [
      { text: "Požičať", action: this.pozicat, param: "ID_COLLECTION" }
    ];
    this.searchFunc(null);
  }

  searchFunc = (text: string | null): void => {
    if (text !== null && text?.length < 1) text = null;
    if (text !== null) {
      text = this.f.webalize(text);
    }
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.http.get("/book/search/" + text).subscribe(
        response => {
          console.log(response);
          this.header = [];
          this.header.push({
            TITLE: "Názov",
            YEAR: "Rok vydania",
            PUBLISHER: "Vydavateľstvo",
            ISBN: "ISBN",
            AUTHOR: "Autor"
          });
          this.data = [];
          this.data = response.data;
          this.loading = false;
        }, 
        error => {}
      );
    }, 500);
  }

  pozicat = (param: number): void => {
    var id = this.route.snapshot.paramMap.get('id');
    this.http.get("/book/loanBook/" + id + "/" + param).subscribe(
      response => {
        console.log(response);
        if (response.success) {
          this.toast.success(response.message);
          this.router.navigate(['/profile/' + id]);
        } else {
          this.toast.error(response.message);
        }
      },
      error => {}
    );
  }

}
