import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pays',
  templateUrl: './pays.component.html',
  styleUrls: ['./pays.component.scss']
})
export class PaysComponent implements OnInit {

  loading: boolean = true;
  platby?: any;

  platbyHeader: any[] = [
    { DESCRIPTION: "Popis", DATE_PAY: "Dátum platby", VALUE: "Hodnota" }
  ];

  constructor(private http: RequestService, private user: UserService, private route: Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      if (!this.user.isLogged()) {
        this.route.navigate(['/home']);
      } else {
        this.http.get("/users/userDetail/" + this.user.getID()).subscribe(
          response => {
            this.platby = response.data.platby;

            this.platby.forEach((item: any) => {
              item.VALUE = item.VALUE + " €";
            });
            this.loading = false;
          },
          error => {}
        );
      }
    }, 1000);
  }

}
