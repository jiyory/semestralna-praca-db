import { Component, OnInit } from '@angular/core';
import { FunctionService } from 'src/app/services/function.service';
import { RequestService } from 'src/app/services/request.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  header: any[] = [];
  data: any[] = [];
  loading: boolean = true;
  timer?: any;

  constructor(private http: RequestService, private f: FunctionService) { }

  ngOnInit(): void {
    this.searchFunc(null);
  }

  searchFunc = (text: string | null): void => {
    if (text !== null && text?.length < 1) text = null;
    if (text !== null) {
      text = this.f.webalize(text);
    }
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.http.get("/users/search/" + text).subscribe(
        response => {
          this.header = [];
          this.header.push({
            ID: "Ident. číslo",
            FULLNAME: "Meno",
            USERTYPE: "Druh",
            CITY: "Mesto",
            ADRESS: "Adresa"
          });
          this.data = [];
          this.data = response.data;
          this.loading = false;
        }, 
        error => {}
      );
    }, 500);
  }

}
