import { AfterContentInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { RequestService } from 'src/app/services/request.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  public constructor(private user: UserService, private req: RequestService, private cookie: CookieService, private route: Router) { }

  public ngOnInit(): void {
    this.cookie.set("SID", "");
    setTimeout(() => {
      this.user.unsetData();
      this.route.navigate(['/home']);
    }, 300);
  }

}
