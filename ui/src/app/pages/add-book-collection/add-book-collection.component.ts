import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FunctionService } from 'src/app/services/function.service';
import { RequestService } from 'src/app/services/request.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-book-collection',
  templateUrl: './add-book-collection.component.html',
  styleUrls: ['./add-book-collection.component.scss']
})
export class AddBookCollectionComponent implements OnInit {

  public file?: Blob;

  title: string = "";
  year: number = 2000;
  isbn: string = "";
  publisher: string = "";
  author: string = "";
  description: string = "";

  public constructor(private func: FunctionService, private http: RequestService, private toast: ToastrService, private route: Router) { }

  public ngOnInit(): void {
  }

  public onChangeFile(event: any): void {
    //console.log(event.target.files[0]); 
    this.func.convertToBinary(event.target.files[0], (data: Blob) => {
      this.file = data;
    });
  }

  submit(): void {
    const data = {
      title: this.title,
      year: this.year,
      isbn: this.isbn,
      publisher: this.publisher,
      author: this.author,
      description: this.description,
      img: this.file
    };
    this.http.post("/book/createCollection", data).subscribe(
      success => {
        if (success.success) {
          this.toast.success(success.message);
          this.clean();
          this.route.navigate(['/detail/' + success.data]);
        } else {
          this.toast.warning(success.message);
        }
      },
      error => { }
    );
  }

  clean(): void {
    this.file = undefined;
    this.title = "";
    this.year = 2000;
    this.isbn = "";
    this.publisher = "";
    this.author = "";
    this.description = "";
  }

}
