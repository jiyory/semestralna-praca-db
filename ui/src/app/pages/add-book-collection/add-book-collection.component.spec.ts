import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBookCollectionComponent } from './add-book-collection.component';

describe('AddBookCollectionComponent', () => {
  let component: AddBookCollectionComponent;
  let fixture: ComponentFixture<AddBookCollectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBookCollectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBookCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
