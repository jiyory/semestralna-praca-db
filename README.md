# KRIMIS - Projekt

# Install
1) stiahni a nainštaluj XAMPP a Oracle XE databázu
# BACKEND/API
2) v xampp/php/php.ini pridaj "extension=php_pdo_oci.dll" bez uvodzoviek, napr. na 2. riadok
3) stiahni a nainštaluj COMPOSER -> pri inštalacii zvoľ xampp/php/php.exe
4) stiahni projekt do xampp/htdocs/sempracdb - ak pouzivas git tak do xampp/htdocs a nazov daj "SempPacDB"
5) spusti cmd, presun sa do zlozky xampp/htdocs/sempracdb/api a zadaj prikaz "composer update" - dostahuje dependency
6) otvor projekt (napr. vo Visual Studio Code) a choď do api/config/local.neon - ak neexistuje, prekopiruj common.neon a premenuj na local.neon
7) otvor local.neon a zmen dns, user a password - dns musi obsahovat IP ADRESU!!
	dns: 'oci:dbname=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.35)(PORT=1521))(CONNECT_DATA=(SID=xe)))'
8) spusti xampp controll panel a spusti APACHE
	8.1) vypni APACHE
	8.2) klikni na Config a tam Apache (httpd.conf)
	8.3) na koniec suboru pridaj:
		!!! POZOR !!! na webe (gite) sa nezobrazuju zobaky, preto projekt stiahni a otvor si README.md v roote projektu!!! 
		<IfModule mod_headers.c>
			Header set Access-Control-Allow-Origin "*"
		</IfModule>
	8.4) zapni APACHE
9) otvor http://localhost/api/www - malo by otvorit nejaku stranku a nemalo by vypisat ziaden error
# FRONTEND
11) stiahni a nainštaluj node.js
12) otvor CMD a nainštaluj angular pomocou prikazu "npm install -g @angular/cli"
15) ak este nemas, tak stiahni projekt do xampp/htdocs/sempracdb - ak pouzivas git tak do xampp/htdocs a nazov daj "sempracdb"
16) otvor cmd, presun sa do xampp/htdocs/sempracdb/ui a spusti prikaz "npm update"
17) otvor cmd a presun sa do xampp/htdocs/sempracdb/ui a spusti angular develop server pomocou "ng serve --open" - mozes pouzit aj run.bat subor v projekte
18) do prehliadača si nainšaluj plugin na povolenie CORS (Allow CORS pre chrome) - inak ti nepôjdu AJAX requesty na localhost
# BUILD
- backend
- frontend: "ng build"

# Pouzitie
PHP: $this->template->nejaka_premenna
	HTML:	{$nejaka_premenna}

	$data = $this->database
			->query("SELECT * FROM test_table")->fetchAll();

	$this->template->test = $data[2]->DATA2;