<?php

//declare(strict_types=1);

namespace App;

use Nette\Application\UI\Presenter;

final class CORS
{
    private $presenter;

	public function __construct(Presenter $pres) {
        $this->presenter = $pres;
     }

    /*
     * POVOLI CORS PRE POST
     */
    public function allowCors(): void {
        $res = $this->presenter->getHttpResponse();
        $req = $this->presenter->getHttpRequest();
        $res->setHeader('Access-Control-Allow-Origin', $req->getHeader('Origin'));
        $res->setHeader('Access-Control-Allow-Credentials', 'true');
        $res->setHeader('Access-Control-Allow-Methods', 'POST, GET');
        $res->setHeader('Access-Control-Allow-Headers', 'SID, Accept, Overwrite, Destination, Content-Type, Depth, User-Agent, Translate, Range, Content-Range, Timeout, X-Requested-With, If-Modified-Since, Cache-Control, Location');
        session_destroy();
        session_id($req->getHeader("SID"));
        session_start();
    }

    /*
        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                // if POST
                break;
            case 'GET':
            case 'PATCH':
                // if GET || POST
                break;
            default:
                // else
                break;
        }
    */
}
