<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CORS;
use Nette;
use Nette\Application\UI\Presenter;
use Nette\Database\Connection;
use Nette\Http\Response;
use Nette\Utils\Json;
use stdClass;

final class BookPresenter extends Presenter
{
    private $database;
    private $cors;

	public function __construct(Connection $database)
	{
		$this->database = $database;
        $this->cors = new CORS($this);
	}

    public function actionSearch(string $key): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->searchBooks($res, $key));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionDetailBook(string $key): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->detailBook($res, $key));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionLoanBook(int $key, int $key2): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->loanBook($res, $key, $key2));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionReserveBook(string $key, string $key2): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->reserveBook($res, $key, $key2));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionReturnBook(string $key, string $key2): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->returnBook($res, $key, $key2));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionCreateCollection(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                $body = Json::decode($req->getRawBody());
                $this->sendJson($this->createCollection($res, $body));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionAddBook(string $id_rack, int $policka, string $id_collection): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->addBook($res, $id_rack, $policka, $id_collection));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionRemoveBook(string $id_book): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->removeBook($res, $id_book));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    private function searchBooks(Response $res, string $key): Object {
        $resp = new stdClass();
        if ($key == "null") {
            $key = "";
        }
        $key = strtolower($key);
        $word = "%".$key."%";
        $word = str_replace("-", " ", $word);
        $data = $this->database->query("
                SELECT 
                id_collection, 
                bc.book_coll.title as title, 
                bc.book_coll.year as year, 
                bc.book_coll.isbn as isbn, 
                bc.book_coll.description as description, 
                bc.book_coll.publisher as publisher,
                column_value as author,
                img_file
                FROM bookcollection bc, table(bc.authors)
                WHERE LOWER(TRANSLATE(bc.book_coll.title, 'ľščťžýáíéúäňôřěĽŠČŤŽÝÁÍÉÚŇŘĎŤďĺ', 'lsctzyaieuanoreLSCTZYAIEUNRDTdl')) LIKE ?
                OR LOWER(TRANSLATE(bc.book_coll.description, 'ľščťžýáíéúäňôřěĽŠČŤŽÝÁÍÉÚŇŘĎŤďĺ', 'lsctzyaieuanoreLSCTZYAIEUNRDTdl')) LIKE ?
                OR LOWER(TRANSLATE(column_value, 'ľščťžýáíéúäňôřěĽŠČŤŽÝÁÍÉÚŇŘĎŤďĺ', 'lsctzyaieuanoreLSCTZYAIEUNRDTdl')) LIKE ?
                ORDER BY title FETCH NEXT 100 ROWS ONLY
                ", $word, $word, $word);
        $out = [];
        while ($row = $data->fetch()) {
            if ($row->IMG_FILE != null) {
                $row->image = stream_get_contents($row->IMG_FILE, -1, 0);
            }
            unset($row->IMG_FILE);
            array_push($out, $row);
        }

        $resp->data = $out;
        $res->setCode(200);
        return $resp;
    }

    private function detailBook(Response $res, string $key): Object {
        $resp = new stdClass();

        $data = new stdClass();
        $data->profile = $this->database->query("
            SELECT 
            id_collection, 
            bc.book_coll.title as title, 
            bc.book_coll.year as year, 
            bc.book_coll.isbn as isbn, 
            bc.book_coll.description as description, 
            bc.book_coll.publisher as publisher,
            column_value as author,
            img_file
            FROM bookcollection bc, table(bc.authors)
            WHERE id_collection = ?
                ", $key)->fetchAll();
        $data->reservation = $this->database->query("
            select us.userdata.first_name || ' ' || us.userdata.last_name as fullname, us.mail,
            us.userdata.city as city, us.userdata.address as address, us.userdata.postal_code as psc,
            date_from, id_user
            from reservation
            join user_b us using(id_user)
            where id_collection = ? and closed is null
                ", $key)->fetchAll();
        $data->nevratene = $this->database->query("
            select
                u.userdata.last_name || ' ' || u.userdata.first_name as fullname,
                mail,
                id_user,
                date_from,
                date_to,
                id_book
                from book join loan using(id_book) join user_b u using(id_user) where id_collection = ? and return_date is null
                order by date_from DESC
                ", $key)->fetchAll();
        $data->historia = $this->database->query("
            select
                u.userdata.last_name || ' ' || u.userdata.first_name as fullname,
                mail,
                id_user,
                date_from,
                date_to,
                return_date
                from book join loan using(id_book) join user_b u using(id_user) where id_collection = ? and return_date is not null
                order by date_from DESC
                ", $key)->fetchAll();
        $data->inLibrary = $this->database->query("
                select * from book bk join rack using(id_rack) where id_collection = ? and not exists
                    (select 'x' from loan ln where return_date is null and ln.id_book = bk.id_book)
                    order by label
                ", $key)->fetchAll();

        if ($data->profile[0]->IMG_FILE != null) {
            $data->profile[0]->image = stream_get_contents($data->profile[0]->IMG_FILE, -1, 0);
        }
        unset($data->profile[0]->IMG_FILE);

        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function loanBook(Response $res, int $key, int $key2): Object {
        $resp = new stdClass();
        $resp->success = false;
        $data = $this->database->query("
                select id_book as ID_BOOK from bookcollection
                join book bk using(id_collection)
                where not exists (select * from loan ln where return_date is null and ln.id_book = bk.id_book)
                and id_collection = ? and rownum = 1
                ", $key2)->fetchAll();
        if (count($data) > 0) {
            $data = $data[0];
            $resBook  = $this->database->query("
                insert into loan values(sysdate, sysdate + 14, null, ?, ?, null)
            ", $data->ID_BOOK, $key);
            if($resBook->getRowCount() > 0) {
                $resBook  = $this->database->query("
                    update reservation set closed='Y' where id_collection = ? and id_user = ? and closed is null
                ", $key2, $key);
                $resp->success = true;
                $resp->message = "Kniha požičaná.";
            } else {
                $resp->message = "Nastal problém. Skúste obnoviť stránku.";
            }
        } else {
            $resp->message = "Nie je voľná kniha.";
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function reserveBook(Response $res, string $key, string $key2): Object {
        $resp = new stdClass();
        $resp->success = false;
        $data = $this->database->query("
                select * from reservation where id_user = ? and id_collection = ? and closed is null
                ", $key,$key2)->fetchAll();
        if (count($data) == 0) {
            $resBook  = $this->database->query("
                insert into reservation values(sysdate, ?, ?, null)
            ", $key2,$key);
            if($resBook->getRowCount() > 0) {
                $resp->success = true;
            }
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function returnBook(Response $res, string $key, string $key2): Object {
        $resp = new stdClass();
        $resp->success = false;
        $data = $this->database->query("
                update loan set return_date = sysdate where id_book = ? and id_user = ?
                ", $key2, $key);
        if ($data->getRowCount() > 0) {
            $resp->success = true;
            $resp->message = "Kniha vrátena do knižnice.";
        } else {
            $resp->message = "Nastal problém. Skúste obnoviť stránku.";
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function createCollection(Response $res, $data): Object {
        $resp = new stdClass();
        $resp->success = false;
        $response = $this->database->query("
            insert into bookcollection values(null, bookcollection_obj(?, ?, ?, ?, ?), null, t_authors(?))
            ", $data->title, $data->year, $data->isbn ,$data->description, $data->publisher, $data->author);
        if ($response->getRowCount() > 0) {
            $resp->success = true;
            $resp->message = "Kolekcia bola vytvorená.";
            $id = $this->database->query("select max(id_collection) as id from bookcollection")->fetch();
            if (isset($data->img)) {
                $this->updateImage($data->img ,$id->ID);
            }
            $resp->data = $id->ID;
        } else {
            $resp->message = "Nastal problém. Skúste obnoviť stránku.";
        }
        $res->setCode(200);
        return $resp;
    }

    private function updateImage($img, $id): void {
        $arr = [];
        $i = 0;
        $strWrite = "";
        while (strlen($img) > $i * 30000) {
            $count = strlen($img) - $i * 30000;
            if ($count > 30000) {
                $count = 30000;
            }
            array_push($arr, substr($img, $i * 30000, $count));
            $strWrite .= "dbms_lob.writeappend(c, ".$count.", ?);";
            ++$i;
        }
        $this->database->query("
        declare
            c CLOB;
        begin
            UPDATE bookcollection SET img_file = empty_clob() WHERE id_collection LIKE ? 
            RETURNING img_file INTO c;
        
            ".$strWrite."
    
            commit;
        end;
        ", $id, ...$arr);
    }

    private function addBook(Response $res, string $id_rack, $policka, string $id_collection): Object {
        $resp = new stdClass();
        $resp->success = false;
        $dataR = $this->database->query("select id_rack from rack where label like ? and level_of = ?", $id_rack, $policka)->fetchAll();
        if (count($dataR) > 0) {
            $data = $this->database->query("
                insert into book values(null, ?, ?)
                ", $id_collection, $dataR[0]->ID_RACK);
            if ($data->getRowCount() > 0) {
                $resp->success = true;
                $resp->message = "Kniha úspešne pridaná.";
            } else {
                $resp->message = "Nastal problém. Skúste obnoviť stránku.";
            }
        } else {
            $resp->message = "Zadaná polička neexistuje!";
        }
        $res->setCode(200);
        return $resp;
    }

    private function removeBook(Response $res, string $id_book): Object {
        $resp = new stdClass();
        $resp->success = false;
        $resp->data = $id_book;
        $this->database->query("
            delete from loan where id_book = ?
            ", $id_book);
            
        $data = $this->database->query("delete from book where id_book = ?", $id_book);
        if ($data->getRowCount() > 0) {
            $resp->success = true;
            $resp->message = "Kniha úspešne vymazaná.";
        } else {
            $resp->message = "Nastal problém. Skúste obnoviť stránku.";
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

}

//insert into bookcollection values (null, bookcollection_obj('Clara Callan', '2001', '2005018', 'HarperFlamingo Canada', 'In a small town in Cana'));