<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CORS;
use Nette\Application\UI\Presenter;
use Nette\Database\Connection;
use Nette\Http\Response;
use Nette\Utils\FileSystem;
use Nette\Database\DriverException;
use Nette\IOException;
use stdClass;

final class ListPresenter extends Presenter
{
    private $database;
    private $cors;

	public function __construct(Connection $database)
	{
		$this->database = $database;
        $this->cors = new CORS($this);
	}

    public function actionMostLoanedBook(int $year, int $month = -1): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->mostLoanedBook($res, $year, $month));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionMostLoanedAuthor(int $year, int $month = -1): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->mostLoanedAuthor($res, $year, $month));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionMostLoanedCategory(int $year, int $month = -1): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->mostLoanedCategory($res, $year, $month));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionMostLoanedBooksByTime(int $year, int $month = -1): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->mostLoanedBooksByTime($res, $year, $month));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionCountBookByCategory(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->countBookByCategory($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionCountPublishedBookByCategory(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->countPublishedBookByCategory($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionPublishedBookInCategoryByTime(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->publishedBookInCategoryByTime($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionLoanedBookByUser(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->loanedBookByUser($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionMostNoreturnedBook(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->mostNoreturnedBook($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionMostTimeLoanedBook(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->mostTimeLoanedBook($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    private function mostLoanedBook(Response $res, $year, $month): Object {
        $resp = new stdClass();
        if ($month == -1) {
            $data = $this->database->query("
            select b.book_coll.title, row_number() over(order by count(id_book) desc) as rownumber, count(id_book) as count  from bookcollection b
                join book using(id_collection)
                join loan using(id_book)
            where extract(year from date_from) = ? and b.book_coll.title is not null
            group by b.book_coll.title
            FETCH NEXT 100 ROWS ONLY
            ", $year)->fetchAll();
        } else {
            $data = $this->database->query("
            select b.book_coll.title, row_number() over(order by count(id_book) desc) as rownumber, count(id_book) as count  from bookcollection b
                join book using(id_collection)
                join loan using(id_book)
            where extract(year from date_from) = ? and extract(month from date_from) = ? and b.book_coll.title is not null
            group by b.book_coll.title
            FETCH NEXT 100 ROWS ONLY
            ", $year, $month)->fetchAll();
        }
        
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function mostLoanedAuthor(Response $res, $year, $month): Object {
        $resp = new stdClass();
        if ($month == -1) {
            $data = $this->database->query("
                select auth.column_value, row_number() over(order by count(id_book) desc) as rownumber, count(id_book) as count from bookcollection 
                    join book using(id_collection) 
                    join loan using(id_book), 
                table(bookcollection.authors) auth
                where auth.column_value is not null and extract(year from date_from) = ?
                group by auth.column_value
                FETCH NEXT 100 ROWS ONLY
                ", $year)->fetchAll();

        } else {
            $data = $this->database->query("
                select auth.column_value, row_number() over(order by count(id_book) desc) as rownumber, count(id_book) as count from bookcollection 
                    join book using(id_collection) 
                    join loan using(id_book), 
                table(bookcollection.authors) auth
                where auth.column_value is not null and extract(year from date_from) = ? and extract(month from date_from) = ?
                group by auth.column_value
                FETCH NEXT 100 ROWS ONLY
                ", $year, $month)->fetchAll();
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function mostLoanedCategory(Response $res, $year, $month): Object {
        $resp = new stdClass();
        if ($month == -1) {
            $data = $this->database->query("
                select cat_name,  row_number() over(order by count(date_from) desc) as rownumber, count(date_from) as pocet_pozicani from bookcollection b
                    join book using(id_collection)
                    join loan using(id_book)
                    join categoryforbook using (id_collection)
                    join category using(id_category)
                where extract(year from date_from) = ?
                group by cat_name
                ", $year)->fetchAll();

        } else {
            $data = $this->database->query("
                select cat_name,  row_number() over(order by count(date_from) desc) as rownumber, count(date_from) as pocet_pozicani from bookcollection b
                    join book using(id_collection)
                    join loan using(id_book)
                    join categoryforbook using (id_collection)
                    join category using(id_category)
                where extract(year from date_from) = ? and extract(month from date_from) = ?
                group by cat_name
                ", $year, $month)->fetchAll();
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function mostLoanedBooksByTime(Response $res, $year, $month): Object {
        $resp = new stdClass();
        if ($month == -1) {
            $data = $this->database->query("
            select u.userdata.first_name || ' ' || u.userdata.last_name as fullname, count(id_book) as pocet, row_number() over(order by count(id_book) desc) as rn from user_b u
            join loan using (id_user)
                where extract(year from date_from) = ?
                group by u.userdata.first_name || ' ' || u.userdata.last_name
                FETCH NEXT 100 ROWS ONLY
            ", $year)->fetchAll();

        } else {
            $data = $this->database->query("
            select u.userdata.first_name || ' ' || u.userdata.last_name as fullname, count(id_book) as pocet, row_number() over(order by count(id_book) desc) as rn from user_b u
            join loan using (id_user)
                where extract(year from date_from) = ? and extract(month from date_from) = ?
                group by u.userdata.first_name || ' ' || u.userdata.last_name
                FETCH NEXT 100 ROWS ONLY
            ", $year, $month)->fetchAll();
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function countBookByCategory(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select cat_name as kategoria, count(id_book) as pocet_knih, row_number() over(order by count(id_book) desc) as rownumber from bookCollection
                join book using(id_collection)
                join categoryForBook using(id_collection)
                join category using(id_category)
            group by cat_name
            order by pocet_knih desc
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function countPublishedBookByCategory(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select nvl(b.book_coll.year, 2010) as rok, cat_name as kategoria, count(id_book) as pocet_knih from bookCollection b
                join book using(id_collection)
                join categoryForBook using(id_collection)
                join category using(id_category)
            group by b.book_coll.year, cat_name
            order by 1 desc, 3 desc
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function publishedBookInCategoryByTime(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            with
            rank_pocet as (select row_number() over(partition by  b.book_coll.year order by count(id_collection) desc) as rn, b.book_coll.year, cat_name, count(id_collection) from bookcollection b
                join categoryforbook using (id_collection)
                join category using (id_category)
                group by b.book_coll.year, cat_name
                order by b.book_coll.year desc nulls last, rn)
            select * from rank_pocet where rn = 1
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function loanedBookByUser(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            with
                zoradene as (
                select type as typ_uzivatela, cat_name as kategoria, row_number() over(partition by type order by count(date_from) desc) as rn, count(date_from) as pocet_pozicani from category
                    join categoryForBook using(id_category)
                    join bookCollection using(id_collection)
                    join book using(id_collection)
                    join loan using(id_book)
                    join user_b using(id_user)
                    join usertype using(id_usertype)
                group by cat_name, type)
                select rn, typ_uzivatela, kategoria, pocet_pozicani from zoradene
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function mostNoreturnedBook(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            with 
                zoradene as (
                select b.book_coll.title as titul, sum(case when date_to < return_date then 1 else 0 end) as pocet_nevratenych from bookCollection b
                    join book using(id_collection)
                    join loan using(id_book)
                where b.book_coll.title is not null
                group by b.book_coll.title
                order by 2 desc, 1)
                select rownum, titul, pocet_nevratenych from zoradene
                FETCH NEXT 100 ROWS ONLY
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function mostTimeLoanedBook(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            with
                zoradene as (
                select b.book_coll.title as titul, sum(case when return_date is null then date_to - date_from else return_date - date_from end) as dni_pozicane from bookCollection b
                    join book using(id_collection)
                    join loan using(id_book)
                where b.book_coll.title is not null
                group by b.book_coll.title
                order by 2 desc, 1)
                select rownum, titul, dni_pozicane from zoradene
                FETCH NEXT 100 ROWS ONLY
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }
}