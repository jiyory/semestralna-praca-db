<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CORS;
use Nette\Application\UI\Presenter;
use Nette\Database\Connection;
use Nette\Http\Response;
use Nette\Utils\FileSystem;
use Nette\Database\DriverException;
use Nette\IOException;
use stdClass;

final class DBPresenter extends Presenter
{
    private $database;
    private $cors;

	public function __construct(Connection $database)
	{
		$this->database = $database;
        $this->cors = new CORS($this);
	}

    public function actionUpdate(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->updateDatabase($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionReset(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->database->query("UPDATE our_sql_version SET version=0");
                $ret = new stdClass();
                $ret->message = "Verzia nastavená na 0.";
                $this->sendJson($ret);
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    /*
     * Vykona changescripty v zlozke api/www/changeScripts
     * vykona len tie, ktore este neboli vykonane (podla zaznamu v DB)
     */
    private function updateDatabase(Response $res): Object {
        $ret = new stdClass();
        $message = "";
        $error = [];
        $ret->success = false;
        $ret->warning = false;
        try {
            $ver = $this->database->query("select version from our_sql_version")->fetch()->VERSION;
        } catch (DriverException $e) {
            $ver = 0;
        }

        try {
            $fData = FileSystem::read("changeScripts/sc".($ver + 1).".sql");
            $fData = trim(preg_replace('/\s\s+/', '', $fData));
            $oldpos = 0;
            $pos = 0;
            do {
                $pos = strpos($fData, ';', $oldpos);
                if ($pos !== false) {
                    $cmd = substr($fData, $oldpos, $pos - $oldpos);
                    try {
                        $this->database->query($cmd);
                    } catch (DriverException $e) {
                        array_push($error, ["sc".($ver + 1).".sql", $cmd, $e]);
                    }
                }
                $oldpos = $pos + 1;
            } while ($pos !== false);
            ++$ver;
            $message = "Update success. Version ".$ver.".";
            $ret->success = true;
        } catch (IOException $e) {
            // no file to update
            if (strlen($message) == 0) {
                $message = "Your database is up to date.";
            }
        }

        if (count($error) > 0) {
            $message = "Update with errors! Version ".$ver.".";
            $ret->warning = true;
        }

        $ret->message = $message;
        $ret->errorRows = $error;
        $ret->ver = $ver;

        $res->setCode(200);
        return $ret;
    }
}