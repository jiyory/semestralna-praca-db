<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Nette\Database\Connection;
use Nette\Utils\Json;
use Nette\Database\DriverException;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private $database;

	public function __construct(Connection $database)
	{
		$this->database = $database;
	}

    /*
     * POVOLI CORS PRE POST
     */
    public function allowCors(): Nette\Http\Response {
        $res = $this->getHttpResponse();
        $res->setHeader('Access-Control-Allow-Origin', $this->getHttpRequest()->getHeader('Origin'));
        $res->setHeader('Access-Control-Allow-Credentials', 'true');
        $res->setHeader('Access-Control-Allow-Methods', 'POST');
        $res->setHeader('Access-Control-Allow-Headers', 'Accept, Overwrite, Destination, Content-Type, Depth, User-Agent, Translate, Range, Content-Range, Timeout, X-Requested-With, If-Modified-Since, Cache-Control, Location');
        return $res;
    }

    // http://localhost/krimis/api/www/homepage alebo http://localhost/krimis/api/www/
    public function renderDefault(): void {
        $data = $this->database
			->query("SELECT * FROM test_table")->fetchAll();

	    $this->template->test = $data[2]->DATA2;
        $this->sendJson($data); // nepotrebuje template
    }

    /*
        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                // if POST
                break;
            case 'GET':
            case 'PATCH':
                // if GET || POST
                break;
            default:
                // else
                break;
        }
    */

    public function actionInsertData(): void {
        $res = $this->allowCors();
        $req = $this->getHttpRequest();
        
        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                $this->createTable(); // TODO nahradit changeScriptom - nevytvarat tabulky takto
                $data = Json::decode($req->getRawBody())->data;
                foreach($data as $row) {
                    $this->database->query(
                        "INSERT INTO temp VALUES (?, ?, ?, ?, ?, ?, ?, to_date(?, 'YYYY-MM-DD HH:MI:SS'), to_date(?, 'YYYY-MM-DD'))",
                        $row->id,
                        $row->district_id,
                        $row->negatives_count,
                        $row->negatives_sum,
                        $row->positives_count,
                        $row->positives_sum,
                        $row->positivity_rate,
                        $row->updated_at,
                        $row->published_on
                    );
                }
                $res->setCode(200);
                $this->sendJson(null);
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    private function createTable() {
        try {
            $this->database->query("
                CREATE TABLE temp(
                    id VARCHAR2(15),
                    district_id NUMBER,
                    negatives_count NUMBER,
                    negatives_sum NUMBER,
                    positives_count NUMBER,
                    positives_sum NUMBER,
                    positivity_rate FLOAT,
                    updated_at DATE,
                    published_on DATE,
                    PRIMARY KEY (id)
                )
            ");
            $this->database->commit();
        } catch (Nette\Database\DriverException $e) {
            // pravdepodobne uz existuje
        }
    }

    // http://localhost/krimis/api/www/homepage/test
    public function actionTest(): void {
        $res = $this->allowCors();
        $req = $this->getHttpRequest();
        
        if ($req->getMethod() == 'POST') {
            $data = $this->database->query("SELECT * FROM test_table")->fetchAll();
            //$body = Json::decode($req->getRawBody(), Json::FORCE_ARRAY); // tu musim pristupovat ako do pola
            //$data[0]->DATA2 = $body['neviem']; // takto
            $body = Json::decode($req->getRawBody());
            $data[0]->DATA2 = $body->neviem;
            $res->setCode(200);
            $this->sendJson($data[0]);
        } else if ($req->getMethod() == 'GET') {
            $res->setCode(404);
            $this->sendJson(null);
        } else {
            $this->sendJson(null);
        }
    }

    // http://localhost/krimis/api/www/homepage/takedata
    // vezme akekolvek poslane data a prevedie ich na INSERT prikaz
    // NEPOUZIVAT!! TOTALNE UNSAFE - treba osetrit sql injecty!!
    // sluzi len na znazornenie ako z dat dostat kluce
    public function actionTakeData(): void {
        $res = $this->allowCors();
        $req = $this->getHttpRequest();
        
        if ($req->getMethod() == 'POST') {
            $body = Json::decode($req->getRawBody());
            $str = 'INSERT INTO table_name (';
            $isFirst = true;
            foreach($body->data[0] as $key => $value) {
                if (!$isFirst) {
                    $str = $str.', ';
                }
                $str = $str.$key;
                $isFirst = false;
            }
            $str = $str.') VALUES (';
            $isFirst = true;
            foreach($body->data[0] as $key => $value) {
                if (!$isFirst) {
                    $str = $str.', ';
                }
                $str = $str.$value;
                $isFirst = false;
            }
            $str = $str.');';
            $res->setCode(200);
            $this->sendJson($str);
        } else if ($req->getMethod() == 'GET') {
            $res->setCode(404);
            $this->sendJson(null);
        } else {
            $this->sendJson(null);
        }
    }

    // http://localhost/krimis/api/www/homepage/header
    public function renderHeader(): void {
        $httpRequest = $this->getHttpRequest();
        $url = $httpRequest->getUrl();
        echo $url."<br>"; // https://doc.nette.org/cs/?action=edit
        echo $url->getHost()."<br>"; // nette.org

        // GET
        $all = $httpRequest->getQuery(); // vrací pole všech parametrů z URL
        $id = $httpRequest->getQuery('test'); // vrací GET parametr 'id' (nebo null)
        echo $id;

        // POST
        $all = $httpRequest->getPost(); // vrací pole všech parametrů z POST
        $id = $httpRequest->getPost('id'); // vrací POST parametr 'id' (nebo null)
    }


}
