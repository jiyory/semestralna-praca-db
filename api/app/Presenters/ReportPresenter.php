<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CORS;
use Nette\Application\UI\Presenter;
use Nette\Database\Connection;
use Nette\Http\Response;
use Nette\Utils\FileSystem;
use Nette\Database\DriverException;
use Nette\IOException;
use stdClass;

final class ReportPresenter extends Presenter
{
    private $database;
    private $cors;

	public function __construct(Connection $database)
	{
		$this->database = $database;
        $this->cors = new CORS($this);
	}

    public function actionOverall(string $from, string $to): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->overall($res, $from, $to));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionPaidByUsertype(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->paidByUsertype($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionPaidByCity(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->paidByCity($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionDebtByUsertype(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->debtByUsertype($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionDebtByCity(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->debtByCity($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionPayByName(string $name = ""): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->payByName($res, $name));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionPayByPaytype(string $paytype = ""): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->payByPaytype($res, $paytype));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionLoanFromTime(string $timeFrom, string $timeTo): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->loanFromTime($res, $timeFrom, $timeTo));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionNewRegistratedFromTime(string $time): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->newRegistratedFromTime($res, $time));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionActiveUserFromTime(string $time): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->activeUserFromTime($res, $time));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionTotalProfit(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->totalProfit($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionProfitByMounthUsertype(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->profitByMounthUsertype($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionProfitByMounth(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->profitByMounth($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionCountPlaceByCategory(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->countPlaceByCategory($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionCountBook(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->countBook($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    private function overall(Response $res, string $from, string $to) {
        $resp = new stdClass();
        $resp->data = new stdClass();

        $data = $this->database->query("
                select count(*) as pozicanychKnih from loan
                where date_from >= to_date(?, 'DD.MM.YYYY') and date_from <= to_date(?, 'DD.MM.YYYY')
                ", $from, $to)->fetch();
        $resp->data->pozicanych = $data->POZICANYCHKNIH;

        $data = $this->database->query("
                select count(*) as registrovany from user_b
                where register_date >= to_date(?, 'DD.MM.YYYY')
                ", $from)->fetch();
        $resp->data->registrovany = $data->REGISTROVANY;

        $data = $this->database->query("
                select count(*) as aktivny from user_b
                where id_user in (select id_user from loan where date_from >= to_date(?, 'DD.MM.YYYY'))
                ", $from)->fetch();
        $resp->data->aktivny = $data->AKTIVNY;

        $data = $this->database->query("
                select sum(value * discount) as ziskCelkom from paytype
                    join pay using(id_type)
                    join user_b using(id_user)
                    join usertype using(id_usertype)
                ")->fetch();
        $resp->data->profit = $data->ZISKCELKOM;

        $data = $this->database->query("
                select count(*) as pocetKnih from book
                ")->fetch();
        $resp->data->knihy = $data->POCETKNIH;

        $res->setCode(200);
        return $resp;
    } 

    private function paidByUserType(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select type, avg(value * discount) as suma from paytype
                join pay using (id_type)
                join user_b using (id_user)
                join usertype using (id_usertype)
            where date_pay <= sysdate 
            group by type
            order by 2 desc
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function paidByCity(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select u.userdata.city as city, avg(value * discount) as suma from paytype
                join pay using (id_type)
                join user_b u using (id_user)
                join usertype using (id_usertype)
            where date_pay <= sysdate 
            group by u.userdata.city
            order by 2 desc
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function debtByUsertype(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select type, avg(value * discount) as suma from paytype
                join pay using (id_type)
                join user_b using (id_user)
                join usertype using (id_usertype)
            where date_pay >= sysdate 
            group by type
            order by 2 desc
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function debtByCity(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select u.userdata.city as city, avg(value * discount) as suma from paytype
                join pay using (id_type)
                join user_b u using (id_user)
                join usertype using (id_usertype)
            where date_pay >= sysdate 
            group by u.userdata.city
            order by 2 desc
                ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function payByName(Response $res, string $name): Object {
        $resp = new stdClass();
        if ($name == "null") {
            $name = "";
        }
        $name = strtolower($name);
        $word = "%".$name."%";
        $data = $this->database->query("
            select date_pay, name, description, value, u.userdata.last_name || ' ' || u.userdata.first_name as fullname, mail from pay 
                join user_b u using (id_user)
                join paytype using (id_type)
            where LOWER(u.userdata.first_name || ' ' || u.userdata.last_name) like ?  
            or LOWER(u.userdata.first_name) like ?
            or LOWER(u.userdata.last_name) like ?
            order by date_pay desc
            FETCH NEXT 100 ROWS ONLY
            ", $word, $word, $word)->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function payByPaytype(Response $res, string $paytype): Object {
        $resp = new stdClass();
        if ($paytype == "null") {
            $paytype = "";
        }
        $paytype = strtolower($paytype);
        $word = "%".$paytype."%";
        $data = $this->database->query("
            select date_pay, name, value, u.userdata.first_name || ' ' || u.userdata.last_name as fullName, mail from pay
                join paytype using(id_type)
                join user_b u using (id_user)
            where name like ?
            order by date_pay desc
            FETCH NEXT 100 ROWS ONLY
            ", $word)->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function loanFromTime(Response $res, $timeFrom, $timeTo): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select count(*) as pozicanychKnih from loan
            where date_from >= to_date(?, 'DD.MM.YYYY') and date_from <= to_date(?, 'DD.MM.YYYY')
            ", $timeFrom, $timeTo)->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function newRegistratedFromTime(Response $res, $time): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select count(*) as registrovany from user_b
            where register_date >= to_date(?, 'DD.MM.YYYY')
            ", $time)->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function activeUserFromTime(Response $res, $time): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select count(*) as aktivny from user_b
            where id_user in (select id_user from loan where date_from >= to_date(?, 'DD.MM.YYYY'))
            ", $time)->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function totalProfit(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select sum(value * discount) as ziskCelkom from paytype
                join pay using(id_type)
                join user_b using(id_user)
                join usertype using(id_usertype)
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function profitByMounthUsertype(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select extract(month from date_pay) || '.' || extract(year from date_pay) as datum, type, sum(value * discount) as zisk from pay
                join user_b using (id_user)
                join usertype using (id_usertype)
                join paytype using (id_type)
            group by extract(month from date_pay) || '.' || extract(year from date_pay), extract(month from date_pay), extract(year from date_pay), type
            order by extract(month from date_pay) desc, extract(year from date_pay) desc, type
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function profitByMounth(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select extract(month from date_pay) || '.' || extract(year from date_pay) as datum, sum(value) as zisk from pay
                join paytype using (id_type)
            group by extract(month from date_pay) || '.' || extract(year from date_pay), extract(month from date_pay), extract(year from date_pay)
            order by extract(month from date_pay) desc, extract(year from date_pay) desc
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function countPlaceByCategory(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            with
                miesta as (select category, count(*)*60 as pct_miest from rack group by category),
                knihy as (select cat_name, count(id_book) as pct_knih from book join bookcollection using(id_collection) join categoryforbook using(id_collection) join category using(id_category) group by cat_name)
                select cat_name, pct_miest - pct_knih as volneMiesta from knihy join miesta on(cat_name = category)
                ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function countBook(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select count(*) as pocetKnih from book
            ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }
}