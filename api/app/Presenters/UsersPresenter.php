<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CORS;
use Nette\Application\UI\Presenter;
use Nette\Database\Connection;
use Nette\Http\Response;
use Nette\Utils\FileSystem;
use Nette\Database\DriverException;
use Nette\IOException;
use stdClass;

final class UsersPresenter extends Presenter
{
    private $database;
    private $cors;

	public function __construct(Connection $database)
	{
		$this->database = $database;
        $this->cors = new CORS($this);
	}

    public function actionSearch(string $key): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->searchUsers($res, $key));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionUserDetail(string $key): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->userDetail($res, $key));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionGetPrivilegeType(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->getPrivilegeTipe($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionGetUserType(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->getUserType($res));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionSetUserTypePrivilage(string $user, string $privilege, string $usertype): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->setUserTypePrivilage($res, $privilege, $usertype, $user));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    private function userDetail(Response $res, string $key): Object {
        $resp = new stdClass();
        $key = strtolower($key);
        $data = new stdClass();
        $data->profile = $this->database->query("
            select id_user as id,
            us.userdata.first_name as name,
            us.userdata.last_name as lastname,
            us.userdata.city as city,
            us.userdata.address as address,
            us.userdata.postal_code as psc,
            mail as mail,
            type as usertype,
            id_usertype,
            name as privilege,
            id_privilege,
            img_file
            from user_b us 
            join privilege using(id_privilege) 
            join usertype using(id_usertype)
            where id_user = ?
        ", $key)->fetchAll();
        $data->platnost = $this->database->query("
            with 
                platnost as (select id_pay from pay where id_user = ? and (SYSDATE - NUMTOYMINTERVAL(1, 'year')) < date_pay and id_type = 1)
                select (case when count(*) > 0 then 'true' else 'false' end) as platnost from platnost
        ", $key)->fetchAll();
        $data->pozicane = $this->database->query("
            select id_collection as colectionId, id_book as bookId, date_from , date_to, bc.book_coll.title as title, bc.book_coll.year as year, 
            bc.book_coll.publisher as publisher  from user_b
            join loan using(id_user)
            join book using(id_book)
            join bookcollection bc using(id_collection)
            where id_user = ? and return_date is null order by date_from desc
        ", $key)->fetchAll();
        $data->rezervovane = $this->database->query("
            select id_collection as colectionId, bc.book_coll.title as title, bc.book_coll.year as year, bc.book_coll.publisher as publisher, date_from from user_b 
            join reservation using(id_user)
            join bookcollection bc using(id_collection)
            where id_user = ? and closed is null order by date_from desc
        ", $key)->fetchAll();
        $data->historia = $this->database->query("
            select id_collection as colectionId, id_book as bookId, date_from, date_to, return_date, bc.book_coll.title as title, bc.book_coll.year as year, bc.book_coll.publisher as publisher  from user_b
            join loan using(id_user)
            join book using(id_book)
            join bookcollection bc using(id_collection)
            where id_user = ? and return_date is not null order by return_date desc
        ", $key)->fetchAll();
        $data->platby = $this->database->query("
            select id_type, id_pay, name, description, value, date_pay from pay
            join paytype using(id_type)
            where id_user = ? order by date_pay desc
        ", $key)->fetchAll();

        if ($data->profile[0]->IMG_FILE != null) {
            $data->profile[0]->image = stream_get_contents($data->profile[0]->IMG_FILE, -1, 0);
        }
        unset($data->profile[0]->IMG_FILE);

        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function searchUsers(Response $res, string $key): Object {
        $resp = new stdClass();
        if ($key == "null") {
            $key = "";
        }
        $key = strtolower($key);
        $word = "%".$key."%";
        $word = str_replace("-", " ", $word);
        $data = $this->database->query("
        select id_user as id,
        us.userdata.last_name || ' ' || us.userdata.first_name as fullname,
        us.userdata.city as city,
        us.userdata.address as adress,
        us.userdata.postal_code as psc,
        mail as mail,
        type as usertype,
        name as privilage
        from user_b us join privilege using(id_privilege) join usertype using(id_usertype)
        where lower(TRANSLATE(us.userdata.last_name || ' ' || us.userdata.first_name, 'ľščťžýáíéúäňôřěĽŠČŤŽÝÁÍÉÚŇŘĎŤďĺ', 'lsctzyaieuanoreLSCTZYAIEUNRDTdl')) like ? OR 
        id_user like ? order by us.userdata.last_name
        ", $word, $word)->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function getPrivilegeTipe(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select * from privilege
        ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function getUserType(Response $res): Object {
        $resp = new stdClass();
        $data = $this->database->query("
            select * from usertype
        ")->fetchAll();
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }

    private function setUserTypePrivilage(Response $res, string $privilage, $usertype, $user): Object {
        $resp = new stdClass();
        $resp->success = false;
        $data = $this->database->query("
            update user_b set id_privilege = ? where id_user = ?
        ", $privilage, $user);
        if($data->getRowCount() < 1) {
            $resp->data = $data;
            $res->setCode(200);
            return $resp;
        }
        $data = $this->database->query("
            update user_b set id_usertype = ? where id_user = ?
        ", $usertype, $user);
        if($data->getRowCount() > 0) {
            $resp->success = true;
        }
        $resp->data = $data;
        $res->setCode(200);
        return $resp;
    }
}