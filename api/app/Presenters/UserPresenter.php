<?php

declare(strict_types=1);

namespace App\Presenters;

use App\CORS;
use Exception;
use Nette\Application\UI\Presenter;
use Nette\Database\Connection;
use Nette\Http\IRequest;
use Nette\Http\Response;
use Nette\Http\Session;
use Nette\Utils\Json;
use stdClass;

final class UserPresenter extends Presenter
{
    private $database;
    private $session;
    private $cors;

	public function __construct(Connection $database, Session $session)
	{
		$this->database = $database;
        $this->session = $session;
        $this->cors = new CORS($this);
	}

    public function actionLoad(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $this->sendJson($this->loadUser());
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionLogin(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                $body = Json::decode($req->getRawBody());
                $this->sendJson($this->loginUser($body));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionRegister(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                $body = Json::decode($req->getRawBody());
                $this->sendJson($this->registerUser($body));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionUpdate(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'POST': 
                $body = Json::decode($req->getRawBody());
                $this->sendJson($this->updateUser($body));
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    public function actionUsertype(): void {
        $req = $this->getHttpRequest();
        $res = $this->getHttpResponse();
        $this->cors->allowCors();

        switch ($req->getMethod()) {
            case 'OPTIONS':
                $this->sendJson(null);
                break;
            case 'GET': 
                $resp = new stdClass();
                $resp->message = "Success";
                $resp->result = true;
                $resp->data = $this->database->query("SELECT * FROM userType")->fetchAll();
                $this->sendJson($resp);
                break;
            default:
                $res->setCode(404);
                $this->sendJson(null);
                break;
        }
    }

    private function registerUser($data): Object {

        $resp = new stdClass();

        if ($this->userExists($data->mail)) {
            $resp->message = "Tento email je už používaný!";
            $resp->created = false;
        } else {
            // TODO kontrola vstupov
            try {
                $this->createUser($data->name, $data->last_name, $data->address, $data->postal_code, 
                    $data->city, $data->mail, hash('sha256', $data->password), 1, $data->usertype);
                $resp->message = "Účet vytvorený. Môžete sa prihlásiť.";
                $resp->created = true;
            } catch (Exception $e) {
                $resp->message = "Nastala chyba. Skúste obnoviť stránku.";
                $resp->created = false;
            }
        }

        return $resp;
    }

    private function createUser($name, $last_name, $address, $postal_code, $city, $mail, $password, $privilege, $usertype): void {
        $this->database->query(
            "INSERT INTO user_b VALUES(NULL, user_obj(?, ?, ?, ?, ?), ?, ?, sysdate, ?, ?, NULL, NULL)",
            $name, $last_name, $address, $postal_code, $city, $mail, $password, $privilege, $usertype
        );
    }

    private function loginUser($data): Object {
        $resp = new stdClass();
        $resp->success = false;
        $result = $this->database->query("SELECT id_user, mail, password, id_usertype, id_privilege, u.userdata.first_name as first_name, u.userdata.last_name as last_name, 
            u.userdata.address as address, u.userdata.postal_code as postal, u.userdata.city as city, img_file
            FROM user_b u WHERE mail LIKE ?", $data->mail)->fetchAll();
        if (count($result) == 0) {
            $resp->message = "Užívateľ s týmto emailom neexistuje!";
        } else if (count($result) > 1) {
            $resp->message = "Vyskytla sa chyba! Kontaktujte správcu.";
        } else {
            if ($result[0]->PASSWORD == hash('sha256', $data->pwd)) {
                $resp->message = "Úspešne prihlásený.";
                $resp->success = true;
                unset($result[0]->PASSWORD);
                if ($result[0]->IMG_FILE != null) {
                    $resp->image = stream_get_contents($result[0]->IMG_FILE, -1, 0);
                }
                unset($result[0]->IMG_FILE);
                $resp->data = $result[0];
                $section = $this->session->getSection("user");
                $section["mail"] = $result[0]->MAIL;
                $resp->sessID = $this->session->getId();
            } else {
                $resp->message = "Nesprávne heslo!";
            }
        }
        return $resp;
    }

    private function loadUser(): Object {
        $resp = new stdClass();
        $resp->success = false;
        $resp->withToast = false;
        $section = $this->session->getSection("user");
        if ($section["mail"] != null) {
            $result = $this->database->query("SELECT id_user, mail, id_usertype, id_privilege, u.userdata.first_name as first_name, u.userdata.last_name as last_name, 
                u.userdata.address as address, u.userdata.postal_code as postal, u.userdata.city as city, img_file
                FROM user_b u WHERE mail LIKE ?", $section["mail"])->fetchAll();

            if (count($result) == 0) {
                $resp->message = "Užívateľ s týmto emailom neexistuje!";
                unset($section["mail"]);
            } else if (count($result) > 1) {
                $resp->message = "Vyskytla sa chyba prihlásenia! Kontaktujte správcu.";
                $resp->withToast = true;
                unset($section["mail"]);
            } else {
                $resp->message = "Úspešne prihlásený.";
                $resp->success = true;
                if ($result[0]->IMG_FILE != null) {
                    $resp->image = stream_get_contents($result[0]->IMG_FILE, -1, 0);
                }
                unset($result[0]->IMG_FILE);
                $resp->data = $result[0];
            }
        } else {
            $resp->message = "Neprihlásený užívateľ.";
        }
        return $resp;
    }

    private function updateUser($data): Object {
        $resp = new stdClass();
        $resp->success = false;
        $section = $this->session->getSection("user");
        if ($section["mail"] != null) {
            $result = $this->database->query("SELECT id_user FROM user_b u WHERE mail LIKE ?", $section["mail"])->fetchAll();

            if (count($result) == 0) {
                $resp->message = "Užívateľ s týmto emailom neexistuje!";
                unset($section["mail"]);
            } else if (count($result) > 1) {
                $resp->message = "Vyskytla sa chyba prihlásenia! Kontaktujte správcu.";
                $resp->withToast = true;
                unset($section["mail"]);
            } else {
                if (isset($data->pwd)) {
                    $rowNums = $this->database->query("UPDATE user_b u SET 
                        mail = ?, password = ?, u.userdata.first_name = ?, u.userdata.last_name = ?, 
                        u.userdata.address = ?, u.userdata.postal_code = ?, u.userdata.city = ?
                        WHERE mail LIKE ?",
                        $data->mail, hash('sha256', $data->pwd), $data->name, $data->lname, $data->address, 
                        $data->postal, $data->city, $section["mail"]);
                } else {
                    $rowNums = $this->database->query("UPDATE user_b u SET 
                        mail = ?, u.userdata.first_name = ?, u.userdata.last_name = ?, 
                        u.userdata.address = ?, u.userdata.postal_code = ?, u.userdata.city = ? WHERE mail LIKE ?",
                        $data->mail, $data->name, $data->lname, $data->address, $data->postal, $data->city, $section["mail"]);
                }
                if ($rowNums->getRowCount() == 1) {
                    if (isset($data->image)) {
                        $this->updateImage($data->image, $data->mail);
                        $resp->image = $data->image;
                    }
                    $result = $this->database->query("SELECT id_user, mail, id_usertype, id_privilege, u.userdata.first_name as first_name, u.userdata.last_name as last_name, 
                        u.userdata.address as address, u.userdata.postal_code as postal, u.userdata.city as city
                        FROM user_b u WHERE mail LIKE ?", $data->mail)->fetchAll();
                    $resp->message = "Nastavenia zmenené.";
                    $resp->success = true;
                    $resp->data = $result[0];
                    $section["mail"] = $data->mail;
                }
            }
        } else {
            $resp->message = "Neprihlásený užívateľ.";
        }
        return $resp;
    }

    private function updateImage($img, $mail): void {
        $arr = [];
        $i = 0;
        $strWrite = "";
        while (strlen($img) > $i * 30000) {
            $count = strlen($img) - $i * 30000;
            if ($count > 30000) {
                $count = 30000;
            }
            array_push($arr, substr($img, $i * 30000, $count));
            $strWrite .= "dbms_lob.writeappend(c, ".$count.", ?);";
            ++$i;
        }
        $this->database->query("
        declare
            c CLOB;
        begin
            UPDATE user_b SET img_file = empty_clob() WHERE mail LIKE ? 
            RETURNING img_file INTO c;
        
            ".$strWrite."
    
            commit;
        end;
        ", $mail, ...$arr);
    }

    private function userExists(string $mail): bool {
        $res = $this->database->query("SELECT count(*) as pct FROM user_b WHERE mail LIKE ?", $mail)->fetch()->PCT;
        if ($res == 0) return false;
        else return true;
    }
}