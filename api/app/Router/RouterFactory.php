<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;
		$router->addRoute('db/update', 'DB:update');
		$router->addRoute('db/reset', 'DB:reset');
		
		$router->addRoute('user/register', 'User:register');
		$router->addRoute('user/usertype', 'User:usertype');
		$router->addRoute('users/search[/<key>]', 'Users:search');
		$router->addRoute('users/userDetail[/<key>]', 'Users:userDetail');
		$router->addRoute('user/update', 'User:update');

		$router->addRoute('user/login', 'User:login');
		$router->addRoute('user/load', 'User:load');
		$router->addRoute('users/getPrivilegeType', 'Users:getPrivilegeType');
		$router->addRoute('users/getUserType', 'Users:getUserType');
		$router->addRoute('users/setUserTypePrivilage/<user>/<privilege>/<usertype>', 'Users:setUserTypePrivilage');

		$router->addRoute('book/search[/<key>]', 'Book:search');
		$router->addRoute('book/detailBook/<key>', 'Book:detailBook');
		$router->addRoute('book/create', 'Book:create');
		$router->addRoute('book/load', 'Book:load');
		$router->addRoute('book/loanBook/<key>/<key2>', 'Book:loanBook');
		$router->addRoute('book/reserveBook/<key>/<key2>', 'Book:reserveBook');
		$router->addRoute('book/returnBook/<key>/<key2>', 'Book:returnBook');
		$router->addRoute('book/createCollection', 'Book:createCollection');
		$router->addRoute('book/addBook/<id_rack>/<policka>/<id_collection>', 'Book:addBook');
		$router->addRoute('book/removeBook/<id_book>', 'Book:removeBook');

		$router->addRoute('report/overall/<from>/<to>', 'Report:overall');
		$router->addRoute('report/paidByUsertype', 'Report:paidByUsertype');
		$router->addRoute('report/paidByCity', 'Report:paidByCity');
		$router->addRoute('report/debtByUsertype', 'Report:debtByUsertype');
		$router->addRoute('report/debtByCity', 'Report:debtByCity');
		$router->addRoute('report/payByName/[<name>]', 'Report:payByName');
		$router->addRoute('report/payByPaytype/[<paytype>]', 'Report:payByPaytype');
		$router->addRoute('report/loanFromTime/<timeFrom>/<timeTo>', 'Report:loanFromTime');
		$router->addRoute('report/newRegistratedFromTime/<time>', 'Report:newRegistratedFromTime');
		$router->addRoute('report/activeUserFromTime/<time>', 'Report:activeUserFromTime');
		$router->addRoute('report/totalProfit', 'Report:totalProfit');
		$router->addRoute('report/profitByMounthUsertype', 'Report:profitByMounthUsertype');
		$router->addRoute('report/profitByMounth', 'Report:profitByMounth');
		$router->addRoute('report/countPlaceByCategory', 'Report:countPlaceByCategory');
		$router->addRoute('report/countBook', 'Report:countBook');


		/*najpožičiavanejšia kniha za rok, mesiac*/
		$router->addRoute('list/mostLoanedBook/<year>/[<month>]', 'List:mostLoanedBook');
		/*najpožičiavanejšia autor za rok, mesiac*/
		$router->addRoute('list/mostLoanedAuthor/<year>/[<month>]', 'List:mostLoanedAuthor');
		/*najpožičiavanejšia kategória za rok, mesiac*/
		$router->addRoute('list/mostLoanedCategory/<year>/[<month>]', 'List:mostLoanedCategory');
		/*najviac požičaných kníh za obdobie rok, mesiac*/ // tu dal pocet knih pozicanych za mesiac
		$router->addRoute('list/mostLoanedBooksByTime/<year>/[<month>]', 'List:mostLoanedBooksByTime');
		/*pocet knih pre kategoriu*/
		$router->addRoute('list/countBookByCategory', 'List:countBookByCategory');
		/*počet knih publikovaných za rok v každej kategórii*/ // tutaj to vypisuje po rokoch pre kazdu kategoriu postupne
		$router->addRoute('list/countPublishedBookByCategory', 'List:countPublishedBookByCategory');
		/*vypis kategorie pre kazdy rok, v ktorej bolo vydanych najviac knih*/
		$router->addRoute('list/publishedBookInCategoryByTime', 'List:publishedBookInCategoryByTime');
		/*rebríček kategórii požičiavaných kníh podľa typu usera*/
		$router->addRoute('list/loanedBookByUser', 'List:loanedBookByUser');
		/*rebríček najnevracanejších kníh*/
		$router->addRoute('list/mostNoreturnedBook', 'List:mostNoreturnedBook');
		/*rebríček najdlhšie požičaných kníh*/
		$router->addRoute('list/mostTimeLoanedBook', 'List:mostTimeLoanedBook');

		
		//$router->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default');
		//$router->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default'); // id je volitelny parameter
		//$router->addRoute('<presenter>/<action>[/<id>/<iid>]', 'Homepage:default'); // id je volitelny parameter, iid tiez, takze bud ziaden alebo oba
		//$router->addRoute('<presenter>/<action>[/<id>[/<iid>]]', 'Homepage:default'); // id je volitelny a iid je volitelnejsi (tj. moze byt 0, 1, alebo oba parametre)
		return $router;
	}
}
