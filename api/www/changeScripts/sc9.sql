insert into reservation with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 3000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 30),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_book as (SELECT rownum as riadok, id_collection FROM bookcollection SAMPLE(10) WHERE ROWNUM <= 3000) select to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_collection, id_user, 'Y' from rand_date join ks_user using(riadok) join rand_book using (riadok);
    
insert into reservation with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 2000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 20),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_book as (SELECT rownum as riadok, id_collection FROM bookcollection SAMPLE(10) WHERE ROWNUM <= 2000) select to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_collection, id_user, 'N' from rand_date join ks_user using(riadok) join rand_book using (riadok);
 
insert into pay with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 2000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 20),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_type as (select rownum as riadok, round(dbms_random.value(1,5)) as id_type, level from dual connect by level <= 2000) select null, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_type, id_user from ks_user join rand_date using (riadok) join rand_type using (riadok);
    
insert into pay with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 2000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 20),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_type as (select rownum as riadok, round(dbms_random.value(1,5)) as id_type, level from dual connect by level <= 2000) select null, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_type, id_user from ks_user join rand_date using (riadok) join rand_type using (riadok);
    
insert into pay with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 2000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 20),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_type as (select rownum as riadok, round(dbms_random.value(1,5)) as id_type, level from dual connect by level <= 2000) select null, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_type, id_user from ks_user join rand_date using (riadok) join rand_type using (riadok);
    
insert into pay with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 2000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 20),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_type as (select rownum as riadok, round(dbms_random.value(1,5)) as id_type, level from dual connect by level <= 2000) select null, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_type, id_user from ks_user join rand_date using (riadok) join rand_type using (riadok);
    
insert into pay with rand_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, 2021 as rok, level from dual connect by level <= 2000),
    rand_user as (SELECT * FROM user_b SAMPLE(10) WHERE ROWNUM <= 100),
    pct_rezervacii as (select level as urov from dual connect by level <= 20),
    ks_user as (select rownum as riadok, id_user from rand_user, pct_rezervacii),
    rand_type as (select rownum as riadok, round(dbms_random.value(1,5)) as id_type, level from dual connect by level <= 2000) select null, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY'), id_type, id_user from ks_user join rand_date using (riadok) join rand_type using (riadok);

 UPDATE our_sql_version SET version=9;