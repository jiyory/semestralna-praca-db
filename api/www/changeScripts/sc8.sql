insert into rack with categ as (select id_category, cat_name from category),
    pocet_rack as (select level as n_rack from dual connect by level <= 45),
    pocet_polic as (select level as n_polic from dual connect by level <= 6),
    oznacenie as (select 'rack_' || id_category || '_' || n_rack as ozn, cat_name, n_polic, rownum as rw from pocet_rack, pocet_polic, categ) select null, ozn, mod(rownum, 3), n_polic, cat_name from oznacenie order by ozn;

insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Umenie, kultúra' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Umenie, kultúra' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;

insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Hobby' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Hobby' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Duchovná literatúra' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Duchovná literatúra' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Cudzie jazyky' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Cudzie jazyky' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Pre deti a mládež' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Pre deti a mládež' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Romantika' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Romantika' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'História' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'História' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Sci-fi, fantasy' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Sci-fi, fantasy' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Detektívka' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Detektívka' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;
    
insert into book with uroven as (select level as urov from dual connect by level <= 4),
    ks as (select round(dbms_random.value(1,3)) as rand, id_collection, cat_name from bookcollection join categoryforbook using (id_collection) join category using (id_category)),
    book as (select rand, id_collection, cat_name from ks, uroven where urov <= rand and cat_name like 'Odborná literatúra' order by id_collection),
    pocet_knih as (select level as cis_knihy from dual connect by level <= 60),
    ks_rack as (select id_rack, category from rack, pocet_knih where category like 'Odborná literatúra' order by category, id_rack),
    zorad_book as (select rownum as pom, id_collection, cat_name from book),
    zorad_rack as (select rownum as pom, id_rack, category from ks_rack) select null, id_collection, id_rack from zorad_rack join zorad_book using(pom) order by id_rack, id_collection;

UPDATE our_sql_version SET version=8;