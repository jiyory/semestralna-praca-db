insert into loan with random_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, round(dbms_random.value(2020,2021)) as rok, level from dual connect by level <= 50000),
    date_from as (select rownum as riadok, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY') as d_from from random_date),
    hranice_book as (select min(id_book) as minim, max(id_book) as maxim from book),
    rand_book as (select rownum as riadok, id_book from (select id_book from book order by dbms_random.value) where rownum <= 50000),
    hranice_user as (select min(id_user) as minim, max(id_user) as maxim from user_b),
    rand_user as (SELECT rownum as riadok, round(dbms_random.value(minim, maxim)) as id_user, level FROM hranice_user connect by level <= 50000),
    date_user as (select rownum as riadok, d_from, d_from + 30 as d_to, id_user from date_from join rand_user using (riadok)) select d_from, d_to, (case when d_to > sysdate then null else d_from + round(dbms_random.value(10,40)) end), id_book, id_user, null from date_user join rand_book using (riadok) order by d_from, id_book;

insert into loan with random_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, round(dbms_random.value(2018,2019)) as rok, level from dual connect by level <= 50000),
    date_from as (select rownum as riadok, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY') as d_from from random_date),
    hranice_book as (select min(id_book) as minim, max(id_book) as maxim from book),
    rand_book as (select rownum as riadok, id_book from (select id_book from book order by dbms_random.value) where rownum <= 50000),
    hranice_user as (select min(id_user) as minim, max(id_user) as maxim from user_b),
    rand_user as (SELECT rownum as riadok, round(dbms_random.value(minim, maxim)) as id_user, level FROM hranice_user connect by level <= 50000),
    date_user as (select rownum as riadok, d_from, d_from + 30 as d_to, id_user from date_from join rand_user using (riadok)) select d_from, d_to, (case when d_to > sysdate then null else d_from + round(dbms_random.value(10,40)) end), id_book, id_user, null from date_user join rand_book using (riadok) order by d_from, id_book;
    
insert into loan with random_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, round(dbms_random.value(2016,2017)) as rok, level from dual connect by level <= 50000),
    date_from as (select rownum as riadok, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY') as d_from from random_date),
    hranice_book as (select min(id_book) as minim, max(id_book) as maxim from book),
    rand_book as (select rownum as riadok, id_book from (select id_book from book order by dbms_random.value) where rownum <= 50000),
    hranice_user as (select min(id_user) as minim, max(id_user) as maxim from user_b),
    rand_user as (SELECT rownum as riadok, round(dbms_random.value(minim, maxim)) as id_user, level FROM hranice_user connect by level <= 50000),
    date_user as (select rownum as riadok, d_from, d_from + 30 as d_to, id_user from date_from join rand_user using (riadok)) select d_from, d_to, (case when d_to > sysdate then null else d_from + round(dbms_random.value(10,40)) end), id_book, id_user, null from date_user join rand_book using (riadok) order by d_from, id_book;
    
insert into loan with random_date as (select rownum as riadok, round(dbms_random.value(1,28)) as den, round(dbms_random.value(1,12)) as mesiac, round(dbms_random.value(2014,2015)) as rok, level from dual connect by level <= 50000),
    date_from as (select rownum as riadok, to_date(to_char(den || '-' || mesiac || '-' || rok), 'DD-MM-YYYY') as d_from from random_date),
    hranice_book as (select min(id_book) as minim, max(id_book) as maxim from book),
    rand_book as (select rownum as riadok, id_book from (select id_book from book order by dbms_random.value) where rownum <= 50000),
    hranice_user as (select min(id_user) as minim, max(id_user) as maxim from user_b),
    rand_user as (SELECT rownum as riadok, round(dbms_random.value(minim, maxim)) as id_user, level FROM hranice_user connect by level <= 50000),
    date_user as (select rownum as riadok, d_from, d_from + 30 as d_to, id_user from date_from join rand_user using (riadok)) select d_from, d_to, (case when d_to > sysdate then null else d_from + round(dbms_random.value(10,40)) end), id_book, id_user, null from date_user join rand_book using (riadok) order by d_from, id_book;


 UPDATE our_sql_version SET version=10;