insert into category values(null, 'Odborná literatúra'); 
insert into category values(null, 'Detektívka'); 
insert into category values(null, 'Sci-fi, fantasy'); 
insert into category values(null, 'História'); 
insert into category values(null, 'Romantika'); 
insert into category values(null, 'Pre deti a mládež'); 
insert into category values(null, 'Cudzie jazyky'); 
insert into category values(null, 'Duchovná literatúra'); 
insert into category values(null, 'Hobby'); 
insert into category values(null, 'Umenie, kultúra'); 

insert into categoryforbook with cat_count as (select count(*) as category_count from category),
        rnd as (select id_collection, round(dbms_random.value(1,category_count)) rn_cat from bookcollection, cat_count),
        categ as (select id_category, rownum as rw from category)
        select id_collection, id_category from rnd join categ on (rn_cat = rw);

UPDATE our_sql_version SET version=7;